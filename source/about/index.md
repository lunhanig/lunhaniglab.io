---
title: Sobre
date: 2020-07-16 10:12:34
---

[Sobre mim](/about/me): informações sobre mim, minha formação acadêmica, links para composições e repositórios

[Sobre este site](/about/site): informações sobre o que trata este site e como foi desenvolvido. 
