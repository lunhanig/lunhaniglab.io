---
title: Sobre este site
date: 2020-07-16 14:22:32
---

Este blog incluirá conteúdos sobre artes, computação e artes computacionais e desenvolvimento de sites

### Como fiz esse site

Este site foi desenvolvido utilizando a ferramenta [Hexo](https://hexo.io/). É uma ferramenta construída em [node.js](https://nodejs.org) muito bacana. Todos os posts são criados em formato [markdown](https://www.markdownguide.org/).
