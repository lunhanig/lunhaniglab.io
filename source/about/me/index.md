---
title: Sobre mim
date: 2020-07-16 11:46:42
---

Meu nome é Guilherme Lunhani. Sou professor de artes em São Bernardo do Campo (SP) - Brasil.

## Formação

Sou formado em educação musical pela UNICAMP com mestrado em Artes, Cultura e Linguagens pela UFJF. Atualmente estou estudando computação pela UNIVESP.

### Foco

O foco da minha formação, como músico, foi em composição assistida por computador e, mais especificamente, em improvisação de códigos  (*livecoding*).

Deste último surgiu interesse por computação. A princípio, sou autodidata em programação, com foco (atualmente) em `node.js` e `python`.

## Soundcloud

Tenho composições publicadas no soundcloud, através de três links:

* [opusd](https://www.soundcoud.com/opusd)
* [sabrina-kawahara](https://www.soundcloud.com/sabrina-kawahara): é um antigo grupo de faculdade que participei, sendo que estes áudios são em colaboração com o cozinheiro Rodrigo Felício.

## Github

Você pode acessar meus códigos [aqui](https://www.github.com/lunhg)
