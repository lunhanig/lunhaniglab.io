---
title: Tópico 103.1 - Trabalhando na linha de comando 29 - recursos de ajuda V
date: 2021-01-11 14:43:15
tags:
    - 103.1
    - ajuda
    - apropos
    - help
---

Esta é a última postagem sobre comandos de ajuda. Veremos agora o comando `apropos`.

<!-- more -->

# Comando `apropos`

O comando `apropos` retorna um comando a partir de uma descrição. Pode ser considerado como o "contrário" do comando `whatis`. Se executarmos os comandos `whatis "whatis"` e `whatis "apropos"`, veremos que:

| Comando   | Descrição                                             | 
| :-------- | ----------------------------------------------------: |
| `whatis`  | mostra descrições das páginas do manual em uma linha  |
| `apropos` | procurar nas páginas do manual por nomes e descrições |

Por outro lado podemos, em um comando ver que os dois comandos podem ser observados utilizando um argumento apropriado para o `apropos`. Algo como `apropos "descrições"` pode retornar:

```bash
user@linux:~$ apropos "descrições"
apropos (1)            - procurar nas páginas do manual por nomes e descrições
whatis (1)             - mostra descrições das páginas do manual em uma linha
```

# Equivalência com o `man`

Lembre-se que `apropos` equivale a `man -k` ou `man --apropos`:

```bash
user@linux:~$ apropos "descrições"
apropos (1)            - procurar nas páginas do manual por nomes e descrições
whatis (1)             - mostra descrições das páginas do manual em uma linha
```

Retorna o mesmo conteúdo que:


```bash
user@linux:~$ man -k "descrições"
apropos (1)            - procurar nas páginas do manual por nomes e descrições
whatis (1)             - mostra descrições das páginas do manual em uma linha
user@linux:~$ man --apropos "descrições"
apropos (1)            - procurar nas páginas do manual por nomes e descrições
whatis (1)             - mostra descrições das páginas do manual em uma linha
```
