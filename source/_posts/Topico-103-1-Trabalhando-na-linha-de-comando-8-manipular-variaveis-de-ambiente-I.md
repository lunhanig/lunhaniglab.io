---
title: Tópico 103.1 - Trabalhando na linha de comando 8 - manipulando variáveis de ambiente - criando variáveis locais
date: 2020-07-23 10:05:02
tags:
  - variável
  - ambiente
  - local
---

Na postagem sobre [referenciamento de *scripts*](/2020/07/21/Topico-103-Linha-de-comando-6-referenciando-scripts/#Referenciando-o-script), comentei que é possível incluir diretórios na variável de ambiente `PATH`. Mas para podermos fazer isso, será necessário aprender um pouco mais sobre variáveis de ambiente e como manipulá-las.

<!-- more -->

## Carregamento do sistema e variáveis de ambiente

No momento que acessamos um `shell` (no nosso caso, um `bash`), um conjunto de variáveis são carregadas de forma automática. Isso é feito para que diversos processos do `shell` sejam corretamente carregados. Tais variáveis são reconhecidas como *variáveis globais*. Abaixo listamos algumas:

| Variável | Representação | Descrição                                                                 |
| :------- | :-----------: | ------------------------------------------------------------------------: |
| `PATH`   | `$PATH`       | Lista os caminhos de procura dos arquivos/comandos executáveis (externos) |
| `USER`   | `$USER`       | Indica quem é o usuário logado                                            |
| `SHELL`  | `$SHELL`      | Indica qual é o `shell` que está em uso                                   |
| `LANG`   | `$LANG`       | Indica qual é o mapa de caracteres e sua língua em uso                    |

Em conjunto com as variáveis globais, existem as variáveis locais, isto é, são variáveis que não são globais e estão disponíveis apenas em uma sessão de `shell`. Quando digo que estão disponíveis apenas em uma sessão de `shell` quero dizer que qualquer processo `shell` diferente do que estamos usando não será capaz de "visualizar" tais variáveis. Por exemplo: executar um novo processo de `shell`, abrirem outra janela um novo emulador de `shell` ou executar um script.

## Criando variáveis locais 

A criação de uma variável em uma sessão de `shell` é bem simples e segue a seguinte estrutura:

| Nome            | atribuição | valor   |
| :-------------- | :--------: | ------: |
| `NOME_VARIAVEL` | `=`        | `valor` |


```bash
user@linux:~$ MINHA_VARIAVEL=valor
user@linux:~$

### OU ###

user@linux:~$ minha_variavel=valor
user@linux:~$

### OU ###

user@linux:~$ Minha_Variavel="valor da variável"

```

### Importante

- Note que na declaração não pode conter o caractere "espaço";
- O nome da variável pode ter letras maiúsculas e/ou minúsculas, bem como o caracter `_`. Mas não terá acentos;
- Valores podem ser letras ou números, inclusive espaços e acentos. No caso de utilizar espaço, é importante que o valor esteja entre áspas;

## Imprimindo o valor das variáveis

Para imprimir o valor da variável que acabamos de criar, podemos usar o mesmo procedimento utilizado na postagem sobre o comando [`echo`](/2020/07/18/Topico-103-Linha-de-comando-3-comando-echo/#Recuperando-o-tipo-de-shell-sendo-usado):

```bash
user@linux:~$ echo $MINHA_VARIAVEL
valor

### OU ###

user@linux:~$ echo $minha_variavel
valor

### OU ###

user@linux:~$ echo $Minha_Variavel
valor da variável
```
