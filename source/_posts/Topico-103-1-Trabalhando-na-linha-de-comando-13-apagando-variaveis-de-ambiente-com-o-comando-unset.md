---
title: Tópico 103.1 - Trabalhando na linha de comando (13) - removendo variáveis de ambiente com o comando unset
date: 2020-08-08 10:41:19
tags:
  - variáveis
  - ambiente
  - global
  - unset
---

Nas últimas postagens, o enfoque ficou para a definição de variáveis locais, globais (com o comando `export`) e visualização das variáveis definidas (com os comando `set` e `env `). Nesta postagem, veremos como remover definições de variáveis.

<!-- more -->

## Comando `unset`

O comando `unset` é bem simples e autoexplicativo: ele remove definições de variáveis de ambiente

| comando | funcionalidade                    |
| :------ | --------------------------------: |
| `set`   | mostra variáveis globais e locais |
| `env`   | mostra variáveis globais          |
| `unset` | remove variáveis                  |

Apenas para esclarecer, o `unset` é um comando interno, portanto ele funcionada da maneira inversa ao `set`:

```bash
user@linux:~$ type set
unset é um comando interno do shell
```

### Exemplo

```bash
user@linux:~$ TESTE=Linux
user@linux:~$ echo $TESTE
Linux
user@linux:~$ set | less
...
SHELL=/bin/bash
TERM=rxvt-unicode-256color
TESTE=Linux
TSBG1=100
:q
user@linux:~$ unset TESTE
user@linux:~$ echo $TESTE

user@linux:~$ set | less
...
SHELL=/bin/bash
TERM=rxvt-unicode-256color
TSBG1=100
:
```


