---
title: Instalando uma versão virtual de uma distribuição Linux no Windows
date: 2020-07-17 11:19:20
tags:
  - ubuntu
  - virtualbox
  - linux
  - windows
---

Neste blog não irei entrar em detalhes de como instalar um Linux. Existem muitos documentos disponíveis na internet sobre o tema. 

No entanto, para quem não conhece nada de Linux, recomendo instalarem uma distribuição específica, o [Ubuntu](https://ubuntu.com/) em uma máquina virtual, isto é, vocẽ irá usar um linux dentro do windows.

<!-- more -->

De forma bastante sucinta, uma [máquina virtual](https://pt.wikipedia.org/wiki/M%C3%A1quina_virtual) é uma duplicata de uma máquina real: ela simula um *hardware* em forma de *software* para que você possa instalar sistemas operacionais de forma isolada em sua máquina física.

## Instalando o virtual box no windows

 
Existem muitas máquinas virutais, sendo que sugiro usarem o [Virtual Box](https://www.virtualbox.org/)

{% youtuber video jiFsN89xE_A %}
{% endyoutuber %}

## Instalando o Ubuntu no virtual box

Abaixo um vídeo sugestivo de como instalar o Ubuntu no Virual Box:

{% youtuber video QbmRXJJKsvs %} 
{% endyoutuber %}
