---
title: Tópico 103.1 - Trabalhando na linha de comando (16) - variáveis de ambiente importantes III - localização no sistema de arquivos
date: 2020-12-15 09:16:27
tags:
  - variáveis
  - ambiente
  - global
  - arquivos
---

Na última postagem, aprendemos um pouco sobre as variáveis que descrevem o usuário, a pasta de trabalho do usuário e a máquina que o usuário está trabalhando. Nesta postagem será descrito um pouco sobre variáveis que descrevem caminhos importantes do sistema de arquivos do linux.

<!-- more -->

## Variável `PATH`


Já vimos um pouco sobre essa variável em [outra postagem](2020/07/21/Topico-103-Linha-de-comando-5-como-o-shell-sabe-que-um-comando-e-externo-A-variavel-de-ambiente-PATH/). Mas é sempre bom relembrar: a variável`PATH` guarda a informação sobre qual é o caminho que o `shell` deve procurar para executar comandos.


## Variável `PWD`

Esta variável guarda qual é o último diretório que você se moveu, ou, em outras palavras, o nome do diretório que está sendo considerado local no momento. É uma variável que muda conforme "navegamos" no sistema de arquivos.

De fato, `PWD` possue estreita relação como o comando `pwd`, que é uma abreviação para *print working directory* (ou _imprima o diretório de trabalho_):

```bash
user@linux~$ echo $PWD
/home/user
user@linux~$ pwd
/home/user
user@linux~$ cd Downloads
user@linux~$ echo $PWD
/home/user/Downloads
user@linux~$ pwd
/home/user/Downloads
```
