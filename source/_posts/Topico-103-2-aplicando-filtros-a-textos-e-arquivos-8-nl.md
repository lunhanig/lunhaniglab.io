---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 8 - nl
date: 2021-01-28 21:09:16
tags:
    - 103.2
    - nl
    - contagem
    - linhas
---

Outro comando que realiza contagem, dessa vez do número de linhas, é o comando `nl`.

<!-- more -->

O comando `nl`, sem argumentos é muito semelhante ao `cat -b` (veja esta [postagem](/2021/01/17/Topico-103-2-aplicando-filtros-a-textos-e-arquivos-1-cat)): ele imprime o conteúdo de um arquivo (ou entrada-padrão), junto com o número de linhas, desconsiderando as linhas em branco.


```bash
user@linux:~$ cat -b alunos.txt
    1 João Almeida 
    2 Maria   Do Rosário
    3 Paulo       Freire
    
    4 Roberto das flores
    5 André batista
    
            
    6 Bruno Rodrigo

user@linux:~$ nl alunos.txt
    1 João Almeida 
    2 Maria   Do Rosário
    3 Paulo       Freire
    
    4 Roberto das flores
    5 André batista
    
            
    6 Bruno Rodrigo
```
