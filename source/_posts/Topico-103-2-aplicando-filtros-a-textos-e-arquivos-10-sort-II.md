---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 10 - sort (II)
date: 2021-01-30 18:30:16
tags:
    - 103.2
    - sort
    - ordenação
---

O `sort`pode ordenar conteúdos de forma reversa.

<!-- more -->

# Ordenando com o comando `sort` de forma reversa

| Opção curta | Opção longa | Descrição           |
| :---------- | :---------: | ------------------: |
| `-r`        | `--reverse` | inverte o resultado |


com o comando `sort -r alunos.txt`, a saída fica:

```bash
user@linux:~$ sort -r alunos.txt

Roberto das flores
Paulo       Freire
Maria   Do Rosário
João Almeida
Bruno Rodrigo
André batista


```

Note que as duas primeiras linhas estão em branco; isso ocorreu porque o o comando `sort -r` inverte a precdência das linhas em branco:


```bash
user@linux:~$ sort -r alunos.txt | cat -n	
     1	Roberto das flores
     2	Paulo       Freire
     3	Maria   Do Rosário
     4	João Almeida
     5	Bruno Rodrigo
     6	André batista
     7	
     8	
```
