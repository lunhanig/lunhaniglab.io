---
title: Tópico 103.1 - Trabalhando na linha de comando 24 - autocompletar I
date: 2021-01-02 17:17:10
tags:
  - shell
  - tab
  - autocompletar
---

Outro recurso do `bash` é o autocompletar, disponível através de dois modos: o autocomplemento de comandos e o autocomplemento de caminhos de arquivos. Em ambos os casos é utilizado a tecla `Tab`.

<!-- more -->

# Autocompletar de comando

Ao iniciar a digitação de um comando, podemos digitar parte da `string` do comando e digitar `Tab`. Se digitarmos o `Tab` uma vez, pode ocorrer duas coisas:

    * Ou o `bash` autocomplementa totalmente o comando; 
    * Ou o `bash` autocomplementa parcialmente o comando;

Vamos usar como exemplo o comando `type`

## Primeiro caso: autocomplementação total do comando

    - digite `ty`
    - digite `Tab`
    - o bash completa o `type`
    - aperte espaço
    - digite outro comando

```bash
user@linux~$ ty   #DIGITA TAB
user@linux~$ type #DIGITA ESPAÇO E OUTRO COMANDO
user@linux~$ type cd
cd é um comando interno do shell
```

Note que, entre o 3o e 4o passos, o `prompt` se mantêm ao lado da letra `e`. Isso ocorre pois existem outros comandos, além do `type`, com este pedaço de `string`. Como foi proposto usar o comando `type`, foi necessário digiar espaço para dar continuidade ao processo.


## Segundo caso: autocomplementação parcial do comando

    - digite `ty`
    - digite `Tab`
    - o bash completa o `type`
    - digite `Tab`
    - o `bash` apresenta vários comandos e cria uma linha vazia e outra linha semelhante ao que escrevemos
    - digite uma letra que diferencia os vários comandos

```bash
user@linux~$ ty   #DIGITA TAB
user@linux~$ type #DIGITA TAB
type    type1afm    typeoutfileinfo     typeset

user@linux~$ types #DIGITA TAB
user@linux~$ typeset
```

