---
title: Tópico 103.1 - Trabalhando na linha de comando 6 - referenciando scripts (I)
date: 2020-07-21 11:07:55
tags:
  - mkdir
  - chmod
  - PATH
---

Nas duas últimas postagens ([aqui](/2020/07/19/Topico-103-Linha-de-comando-4-o-que-e-um-comando-comando-type/) e [aqui](/2020/07/21/Topico-103-Linha-de-comando-5-como-o-shell-sabe-que-um-comando-e-externo-A-variavel-de-ambiente-PATH/)), vimos sobre comandos internos e comandos externos. Hoje e nas próximas postagens falarei um pouco sobre os *scripts*.

<!-- more -->

Comandos internos são aqueles que foram desenvolvidos dentro do `shell`, e comandos externos são aqueles que estão instalados no sistema e são referenciados através da variável de ambiente `PATH`. Mas e o terceiro tipo de comando, os *scripts*?

## Scripts

Também podemos elaborar nossos próprios comandos, que são chamados de [*scripts*](https://pt.wikipedia.org/wiki/Linguagem_de_script).  No entanto, os *scripts* não são nem comandos internos nem comandos externos.

Para entendermos melhor o conceito de *script* e como executá-lo, utilizarei um exemplo prático. Esse exemplo exigirá aprendermos alguns comandos que não estão previstos no método didático. Suponha que você está no diretório `/home/user` e irá criar um subdiretório `/home/user/scripts`. Dentro deste subdiretório existirá um *script* chamado `echo_test.sh` (a extensão `.sh` especifica que é um *script* para serexecutado em um `shell`):

<script src="https://gist.github.com/lunhg/56c68e071603ab3f2ba4bd6e67ec6e75.js?file=echo_test.sh&__inline=true"></script>

### Criando o subdiretório `/home/user/scripts`

Para criarmos um diretório, usamos o comando `mkdir` (*make directory* -- faça um diretório) no diretório `/home/user` (`~`):

| comando  | opções               | argumento                    |
| :------- | :------------------: | ---------------------------: |
| `mkdir`  | <várias disponíveis> | nome ou caminho do diretório |

```bash
user@linux:~$ mkdir /home/user/scripts
user@linux:~$

#### OU ####

user@linux:~$ mkdir scripts
```

### Baixando o script

Realize o *download* do *script* [aqui](https://gist.githubusercontent.com/lunhg/56c68e071603ab3f2ba4bd6e67ec6e75/raw/aef1cc6e4ec051447fd5923d63c021ffdc667707/echo_test.sh), clique com o botão direito do *mouse* em `Salvar Como (Ctrl+S)` e selecione o diretório `/home/user/scripts`. 

### Executando um script

Se tentarmos executar o script `echo_test.sh` utiizando apenas o nome do arquivo, o `bash` acusará um erro:

```bash
user@linux:~$ echo_test.sh
bash: echo_test.sh: comando não encontrado
```

Isto porque o script não é nem um comando interno e nem o diretório `/home/user/scripts` está incluído no `PATH`. 

### Referenciando o script:

Existem três formas de executar esse *script*:

- Executar através do caminho absoluto do *script*
- Executar através do caminho relativo do *script*
- Incluir o diretório `/home/user/scripts` no `PATH`

Nesta postagem, veremos apenas o primeiro método. Iremos ver o segundo método na postagens subsequente. O terceiro método irá exigir alguns conhecimentos, como o de modificar variáveis de ambiente, e ficará para um futuro não tão distante.

#### Executar através do caminho absoluto do *script*

A maneira mais direta, mas mais trabalhosa de executar o *script*, é explicitar sua localização no sistema de arquivos. No nosso caso, supomos que o *script* está localizado em `/home/user/scripts`. Portanto, executamos:

```bash
user@linux:~$ /home/user/scripts/echo_test.sh
bash: /home/user/scripts/echo_test.sh: Permissão negada 
```

O `bash` encontrou o arquivo, mas agora está acusando que não temos permissão de execução do *script*.

##### Alterando permissões de execução do arquivo

Veremos com mais detalhes isso em postagens futuras. Se quiser, sugiro dar uma olhada nestes [verbete](https://pt.wikipedia.org/wiki/Permiss%C3%B5es_de_sistema_de_arquivos). Para fins práticos, iremos adicionar uma *permissão de execução* para o nosso arquivo de *script*:

###### Comando `chmod`

Veremos com mais detalhes este comando em uma futura postagem. Por enquanto é importante entender que `chmod` significa [*change mode*](https://pt.wikipedia.org/wiki/Chmod) -- alterar modo --, e necessita de pelo menos dois argumentos: o novo tipo de modo do arquivo, e o arquivo que será modificado. 

| comando | argumento 1 | argumento 2 |
| :------ | :---------: | ----------: |
| `chmod` | modo        | arquivo     |


No caso, queremos adicionar uma permissão de execução do arquivo `/home/user/echo_test.sh` para o usuário `user`. Uma forma de fazer isso é indicar os simbolos de soma (`+`) concatenado com uma letra que represente execução (`x`, *execute*, ou executar):

```bash
user@linux:~$ chmod +x /home/user/scripts/echo_test.sh
```
Agora poderemos executar o *script* `echo_test.sh` através de seu caminho absoluto:

```bash
user@linux:~$ /home/user/scripts/echo_test.sh
Isto é um teste
```
