---
title: Tópico 103.1 - Trabalhando na linha de comando 4 - o que é um comando - comando type
date: 2020-07-19 16:22:05
tags:
  - comando
  - type
  - echo
  - cd
  - clear
  - tar
---

Nesta postagem veremos o que é um comando e seus tipos.

<!-- more -->

## O que é um comando

No wikipédia podemos ver a a definição de um [comando](https://pt.wikipedia.org/wiki/Comando_(inform%C3%A1tica)) como a representação elementar de uma ação no computador, no nosso caso, no `shell`. Isto é, através de uma palavra, podemos executar uma ação ou um conjunto de ações (isso quer dizer que um comando pode agregar outros comandos).

## Tipos de comandos no shell

Podemos agrupar os comandos de um `shell` Linux em três tipos

- Interno: um comando que faz parte do `shell`;
- Externo: um programa que foi instalado e executado através do `shell`;
- Script: uma sequência lógica de comandos no `shell`.

### Como saber o tipo de um comando: o comando `type`

Para termos conhecimento do tipo de um comando, executamos um comando específico: `type`. A estrutura desse comando é a seguinte:

| comando | argumento |
| :------ | --------: |
| type    | comando   |


#### Comandos internos

Isto é, o argumento do comando `type` é outro comando. Por exemplo, ao digitarmos `type echo`, o `shell` nos retorna qual é o tipo do comando `echo`:


```bash
user@linux:~$ type echo
echo é um comando interno do shell
```

##### Comando `cd`

Outro exemplo, agora com o comando `cd` (*change directory* -- que muda de diretório):

```bash
user@linux:~$ type cd
echo é um comando interno do shell
```

#### Comandos externos

Vamos usar o mesmo processo com o comando `clear` (que "limpa" a tela do shell):

```bash
user@linux:~$ type clear
clear é /usr/bin/clear
```

Isso significa que o comando `clear` é um programa externo, instalado no sistema, e invocável pelo `shell`. 

##### Comando hasheado

Agora execute novamente o comando acima:

```bash
user@linux:~$ type clear
clear está na tabela hash (/usr/bin/clear)
```

A mensagem mudou, dizendo que o comando `clear`, que está em `/usr/bin/clear`, está na tabela `hash` (ou `hashed` -- hasheado). Uma tabela `hash`, ou [tabela de dispersão](https://pt.wikipedia.org/wiki/Tabela_de_dispers%C3%A3o) é uma tabela que memoriza palavras-chaves com valores para uma busca. Na prática isso quer dizer que o comando `clear` será armazenado em uma tabela de hash para uma referência, tornando a execução do comando mais rápida em uma próxima vez. 

##### Comando `tar`

`tar` é um comando usado para compactação de arquivos e veremos mais adiante seu funcionamento. O importante agora é ver que o `tar` é um comando externo ao `shell` e instalado no sistema:

```bash
user@linux:~$ type tar 
tar é /usr/bin/tar
```
