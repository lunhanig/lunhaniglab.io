---
title: Tópico 103.1 - Trabalhando na linha de comando 7 - referenciando scripts (II)
date: 2020-07-22 20:05:20
tags:
  - pwd
  - cd
  - ls  
  - PATH
---

Na [postagem anterior](/2020/07/21/Topico-103-Linha-de-comando-6-referenciando-scripts/), ensinei como executar um *script* usando seu caminho absoluto (eventualmente foi necessário aprendermos um pouco sobre permissões de arquivos). Nesta postagem falarei como executar um *script* usando o método do caminho relativo.

<!-- more -->

### Executar através do caminho relativo do *script*

Anteriormente, supomos que estamos no diretório `/home/user` e queremos executar o *script* `/home/user/scripts/echo_test.sh`. Para executar o *script* usando o método do caminho relativo precisamos:

- Saber em que diretório estamos;
- verificar se o diretório que queremos ir existe no diretório atual;
- podemos, eventualmente, ir até o diretório que queremos;
- verificar se o *script* realmente existe no diretório que chegamos.

#### Saber em que diretório estamos: comando `pwd`

No momento sabemos que estamos em `/home/user`. Seria redundante utilizarmos um comando que confirmasse isso.

Mas, mais adiante, você experenciará o fato de que, após executar muitos comandos, você já "se perdeu" e não sabe aonde você está no sistema de arquivos. É aí que o comando `pwd`, ou *print working directory* (imprima o diretório de trabalho) é muito útil.

```bash
user@linux:~$ pwd
/home/user
```

#### Verificar se o diretório que queremos ir existe no diretório atual

Vamos verificar se o diretório `scripts` existe em `/home/user`. Para isso usamos o comando `ls`:

```bash
user@linux:~$ ls
. 
.. 
scripts
```

Veja que existem 3 referências:

| Referência | Significado                                      |
| :--------- | -----------------------------------------------: |
| `.`        | Diretório atual (`/home/user`)                   |
| `..`       | Diretório anterior (`/home`)                     |
| `scripts`  | Diretório que queremos ir (`/home/user/scripts`) |

#### Ir até o diretório que queremos: comando `cd`

Para irmos para o diretório `/home/user/scripts`, precisamos *mudar de diretório*, em inglês, *change directory*, ou simplismente `cd`.

| comando | argumento            |
| :------ | -------------------: |
| `cd`    | diretório de destino |

O argumento diretório de destino pode ser *absoluto* ou *relativo*. No nosso caso, elas terão o mesmo resultado

##### Argumento absoluto

```bash
user@linux:~$ cd /home/user/scripts 
user@linux:~/scripts$
```

Note que o bash alterou o seu *prompt* para `~/scripts`. Isso indica que estamos em `/home/user/scripts`. Mas suponha que você quer confirmar:

```bash
user@linux:~/scripts$ pwd
/home/user/scripts
```
##### Argumento relativo

Vamos voltar para o diretório `/home/user`. Para isso podemos user a referência do diretório anterior:

```bash
user@linux:~/scripts$ cd ..
user@linux:~$ pwd
/home/user
```

Agora podemos ir para o diretório *scripts* usando unicamente o nome do diretório, ou concatenando os caracteres `./` ao nome do diretório. Aproveite e também tenha certeza que mudou de diretório (com o comando `pwd`):

```bash
user@linux:~$ cd scripts
user@linux:~/scripts$ pwd
/home/user/scripts
user@linux:~/scripts$

####### OU ########

user@linux:~$ cd ./scripts
user@linux:~/scripts$ pwd
/home/user/scripts
user@linux:~/scripts$
```

#### Verificando se o *script* realmente existe no diretório que chegamos

Vamos voltar a user o comando `ls`, agora no diretório `/home/users/scripts`:


```bash
user@linux:~/scripts$ ls
.
..
echo_test.sh
```

Você notará que o `bash` retorna três referências:

| arquivo        | significado |
| :------------- | ----------: |
| `.`            | É o arquivo que representa de forma relativa o próprio diretório (`/home/user/script`) |
| `..`           | É o arquivo que representa de forma relativa o diretório anterior (`/home/user`) |
| `echo_test.sh` | É o arquivo que queremos executar |

Para executar o script, necessitamos, obrigatóriamente, indicar o caminho relativo com os caracteres `./`:

```bash
user@linux:~/scripts ./echo_test
Isto é um teste
```
