---
title: Tópico 103.1 - Trabalhando na linha de comando 23 - navegando no histórico IV
date: 2020-12-30 17:03:47
tags:
  - shell
  - comandos
  - history
  - search
---

Nessa postagem daremos continuidade ao assunto de propriedades do histórico. Mais especificamente, aprenderemos sobre o `reverse-i-search`.
 
<!-- more -->

Existe uma forma mais simples de procurar por comandos, no `bash`, se comparado com a proposição de usar o `history`: é o `reverse-i-search` (procura incrementada reversa). É [bastante simples](http://notes.jerzygangi.com/reverse-i-search-in-the-linux-shell/):

  - Para começar a procurar, pressione `ctrl+r`;
  - então, digite o começo do comando que você está procurando;
  - se o primeiro resultado não é o que você quer, pressione `ctrl+r` novamente;
  - quando você encontrar o comando que quer, pressione `Enter` para executá-lo

Por exemplo:

  - Digite `ctrl+r`
  - digite `l`
  - pressione `ctrl+r` até encontrar um comando semelhante à `ls`
  - pression `enter`

```bash
user@linux~$ 
(reverse-i-search)`l': ls
(enter)
/usr/bin/ls
. .. 
user@linux/tmp$
```
