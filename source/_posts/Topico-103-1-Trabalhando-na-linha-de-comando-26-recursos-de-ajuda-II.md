---
title: Tópico 103.1 - Trabalhando na linha de comando 26 - recursos de ajuda II
date: 2021-01-09 15:58:31
tags:
    - 103.1
    - ajuda 
    - man
    - apropos
    - whatis
    - help
---

Na postagem passada vimos uma utilização elementar do comando `man` (manual). Nesta postagem será explorada uma opção útil do `man` para procurar manuais a partir de um tema específicado.

 <!-- more -->

# Comando `man` com opção `-k`

A opção `-k` (forma curta) ou `--apropos` procura, a partir de uma proposição, nomes e descrições de comandos. De fato, o comando `man` acompanhado da opção `-k` ou `--apropos`, segundo a descrição de ajuda do `man --help` é equivalente ao comando `apropos` (que veremos em outra postagem).

| Comando | Opção       | Argumento     |
| :------ | :---------: | ------------: | 
| `man`   | `-k`        | `"proposição"`| 
| `man`   | `--apropos` | `"proposição"`|

Tal sintaxe retorna uma lista com um ou mais nomes e descrições contendo a proposição dada.

## Exemplo 

```bash
user@linux:~$ man -k "system update"
sync (3p)           - schedule file system updates
user@linux:~$ man --apropos "version control"
Git (3pm)           - Perl interface to the Git version control system
package (n)         - Facilities for package loading and version control
```

# Comando `man` com opção `-f`

A opção `-f` (forma curta) ou `--whatis` procura, a partir de um comando, descrições sobre o que é (ou são) tal (ou tais) e comando(s). De fato, o comando `man` acompanhado da opção `-f` ou `--whatis`, segundo a descrição de ajuda do `man --help` é equivalente ao comando `whatis` (que veremos em outra postagem).

| Comando | Opção       | Argumento     |
| :------ | :---------: | ------------: | 
| `man`   | `-f`        | `"comando"`| 
| `man`   | `--whatis`  | `"comando"`|

Tal sintaxe retorna uma lista com um ou mais nomes e informações do comando dado.

## Exemplo 

```bash
user@linux:~$ man -f "ls"
ls (1)              - list directory contents
ls (1p)             - list directory contents 
user@linux:~$ man --whatis "man"
man (1)             - um ambiente para os manuais de referência do sistema
man (1p)            - display system documentation
man (7)             - macros to format man pages
```
