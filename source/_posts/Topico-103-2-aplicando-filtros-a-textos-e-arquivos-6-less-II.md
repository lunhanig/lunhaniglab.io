---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 6 - less (II)
date: 2021-01-22 18:33:51
tags:
    - 103.2
    - less 
    - pipe
    - filtros
---

Com arquivos de texto muito grandes, se usarmos o comando `cat`, é provável que será necessário usar a barra de rolagem para lermos conteúdos no meio ou final do arquivo (supondo que estamos usando o terminal dentro de um ambiente gráfico). Mas em casos onde usamo o terminal fora do ambiente gráfico, não teremos a barra de rolagem disponível. É aí que o comando `less` pode ser útil.
<!-- more -->

# O Pipe (`|`) e redirecionamento de comandos

O pipe será tema para uma postagem mais detalhada. Mas por hora, vamos entender que o _pipe_ (`|`) funciona como um cano de água: ele redireciona a saída de um comando à esquerda para ser a entrada de outro comando à direita.

```bash
user@linux:~$ comando1 | comando2
#################################
# * comando1 produz uma saída
# * | captura a saída do comando1
# * | redireciona como entrada para o comando2
# * comando2 usa a saída do pipe como entrada
#################################
```

# Redirecionando de saída para o `less`

O comando `less` também é muito usado para paginar uma saída muito grande de um comando. Suponha que você programou o seguinte _script_ que imprime a data e duas informações sobre o sistema operacional a cada um segundo. Suponha também que você o deixou rodando por 5  minutos

```bash
#!/bin/env bash

while true; do
    echo "[ $(date) ]: $(uname $(uname -ro)" 
    sleep 1
done
```

Ao invés de executar `./script.sh`, podemos executar `./script.sh | less`, que permitirá paginar o conteúdo impresso:

```bash
user@linux:~$ ./script.sh | less
[ sex 22 jan 201 18:49:58 -03 ]: 1.2.3-linux-1 GNU/Linux
[ sex 22 jan 201 18:49:59 -03 ]: 1.2.3-linux-1 GNU/Linux
[ sex 22 jan 201 18:50:00 -03 ]: 1.2.3-linux-1 GNU/Linux
[ sex 22 jan 201 18:50:01 -03 ]: 1.2.3-linux-1 GNU/Linux
[... mais conteúdo ...]
```

