---
title: Tópico 103.1 - Trabalhando na linha de comando 28 - recursos de ajuda IV
date: 2021-01-10 20:39:44
tags:
    - 103.1
    - ajuda 
    - whatis
    - help
---

Nas postagens passadas vimos como usar o comando `man` (manual) e o comando `info`(informação). Nesta postagem veremos uma comando mais sucinto: `whatis` ("o que é").

 <!-- more -->

# Comando `whatis` 

O comando `whatis` mostra uma descrição de um comando. É como perguntar "o que é tal comando?" (`whatis <comando>`) e a resposta é algo sucinto como "o comando tal é isso":

| Comando | Argumento     |
| :------ | ------------: | 
| `whatis`  | `comando`     | 

## Exemplo

```bash
user@linux:~$ whatis tar
tar  (1)      - an archiving utility
tar  (5)      - format of tape archive files
```

O comando acima usado é uma pergunta: "o que é o tar?" cuja a resposta é dividida em duas partes?

  - o `tar` é um utilitário de arquivamento
  - o `tar` é um formato de arquivos de fita

A segunda descrição ser um pouco confusa. Mas se executarmos o comando `info tar` e seguirmos com a leitura da seção `1 Introduction`, veremos uma explicação interessante:

> GNU 'tar' cria e manipula "arquivos" que são na verdade coleções de muitos arquivos [^1] ; o programa provê aos usuários uma forma organizada e um método sistemático para controlar uma grande quantidade de dados. O nome "tar" originalmente veio da frase "Tape ARchive", mas arquivos não necessitam (e nesses dias, tipicamente não) estar em fitas.

## Equivalência com o `man`

Lembre-se que `whatis` equivale a `man -f` ou `man --whatis`:

```bash
user@linux: $ whatis "whatis"
whatis (1)           - mostra descrições das páginas do manual numa linha
```

Retorna o mesmo conteúdo que:

```bash
user@linux: $ man -f "whatis"
whatis (1)           - mostra descrições das páginas do manual numa linha
user@linux: $ man --whatis "whatis"
whatis (1)           - mostra descrições das páginas do manual numa linha
```

[^1]: O primeiro termo, entre aspas, "arquivo", no texto original é "archives"; pode ser entendido como aquele "arquivo de escritório", armários de metal com gavetas que agrupam diversos documentos organizados. Já o segundo termo "arquivo", no texto original, é "files", que podem ser entendidos como documentos.
