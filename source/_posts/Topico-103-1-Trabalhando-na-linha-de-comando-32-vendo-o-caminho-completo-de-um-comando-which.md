---
title: Tópico 103.1 - Trabalhando na linha de comando 32 - conhecendo o caminho completo dos comandos
date: 2021-01-15 10:57:34
tags:
    - 103.1
    - PATH
    - which
    - caminho
---

Em postagens anteriores ([aqui](/Topico-103-1-Trabalhando-na-linha-de-comando-5-como-o-shell-sabe-que-um-comando-e-externo-A-variavel-de-ambiente-PATH), [aqui](/Topico-103-1-Trabalhando-na-linha-de-comando-6-referenciando-scripts) e [aqui](/Topcio-103-1-Trabalhando-na-linha-de-comando-7-referenciando-scripts-II), falamos um pouco sobre como o `shell` encontra comandos no sistema de arquivos através da variável de ambiente `PATH`, que define caminhos completos de diretórios. Esta postagem irá tratar de um comando relacionado à variável `PATH`, o comando `which`, que mostra o caminho completo de um comando.

<!-- more -->

# Comando `which`

O `which` é um comando que apresenta o caminho completo de um arquivo que está contido em alguns dos diretórios definidos pelo `PATH`. 

| Comando | argumento                           | 
| :------ | ----------------------------------: |
| `which` | nome do arquivo do comando em busca |

O `which`, portanto, é um comando de busca de informações, tal como o `man`, `info`, `whatis` e o `apropos`:

```bash 
user@linux:~$ whatis which
which (1)           - shows the full path of (shell) commands.
user@linux:$ which echo
/usr/bin/echo
user@linux:$ which ls
/usr/bin/ls
user@linux:$ which man
/usr/bin/man
user@linux:$ which info
/usr/bin/info
user@linux:$ which which
/usr/bin/which
```

Note que todos os diretórios acima estão definidos pelo `PATH`. Note também que, se você usar o which com algum comando customizado, ou com um comando interno ao `shell`, ele retorna a informação que o comando não foi encontrado tendo como referência a variável `PATH`:

```bash
user@linux:$ which cd
which: no cd in (/usr/local/bin:/usr/bin/:/bin:/usr/local/sbin)
user@linux:$ which type
which: no type in (/usr/local/bin:/usr/bin/:/bin:/usr/local/sbin)
```
