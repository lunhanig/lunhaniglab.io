---
title: Tópico 103.1 - Trabalhando na linha de comando (18) - variáveis de ambiente importantes IV - variáveis dinâmicas
date: 2020-12-15 11:39:01
tags:
  - variáveis
  - ambiente
  - global
  - shell
  - processos
---

Na última postagem, foi falado sobre variáveis que dizem respeito ao terminal e ao shell. Nesta postagem será descrito um pouco sobre variáveis dinâmicas, que dizem respeito aos processos.

<!-- more -->


## Variável `$$`

O nome desta variável é "Dólar Dólar", e ela mostra a identificação de processo (_process identification_ ou `pid`) que está sendo executado no momento. 

Em termos práticos:

### Em um `shell bash`

Usado na linha de comando, a variável mostra o `pid` da sessão de `shell bash` que estamos usando. 

```bash
user@linux$~ echo $$
1234
```

### Em um _script_

A variável mostra o `pid` do processo executado pelo _script_


```bash
user@linux$~ echo "echo $$" > script.sh
user@linux$~ bash script.sh
1234
```

## Variável `$!`

O nome desta variável é "Dólar Exclamação", e ela mostra o `pid` do último processo que foi executado em _background_. 

### O que é um processo executado em _background_?

Até o momento, todos nossos comandos `shell` foram executados em um "modo" chamado" _foreground_ ou *primeiro plano*. Isto é, durante a execução do comando, o `shell` previne que qualquer outro processo seja executado na sessão atual. 

De maneira oposta, o `shell` não impede que outros processos sejam executados quando damos um comando para um processo ser executado em _background_ ou *segundo plano*. Uma forma de fazer isso é explicitar, após o comando, o caractere `&` (_ampersand_ ou _e comercial_). Após executarmos, será apresentado o `pid` do processo que solicitamos:

```bash
user@linux~$ bash script.sh &
[1] 1235
```

Para mais informações, sugiro a leitura deste pequeno [artigo](https://geek-university.com/linux/background-and-foreground-processes/).

### Vendo o último processo em _background_:

Pronto, agora podemos ver o `pid` do último processo executado em _background_ através da variável `$!`

```bash
user@linux~$ echo $!
1235
```

## Variável `$?`

Este é o "Dólar Interrogação" e mostra o *exit code* do último processo executado. 

### O que é um _exit code_?

Segundo o artigo [Linux and Unix exit code tutorial with examples](https://shapeshed.com/unix-exit-codes/) (grifos nossos):

> Um código de saída, também conhecido como código de retorno, é um código retornado para um processo-pai por um executável. Sem sistemas POSIX o padrão do código de saída é *0 para sucesso* e qualquer número de *1 a 255 para qualquer outra coisa*. Códigos de saída são interpretados por _scripts_ de máquina para se adaptarem em eventos de sucesso ou falha. Se os códigos de saída não estão definidos, o código de saída será o do último comando executado.

### Voltando

Imagine que você executou um simples `echo "Hello World"`. Se, em seguida, executarmos um `echo $?` podemos esperar que teremos um `0`, ou sucesso. De maneira oposta, se executarmos algo que possa dar um erro (por exemplo, executar um _script_ que não existe, e em seguida executarmos um `echo $?`, teremos um `1`, que sinaliza uma falha:

```bash
user@linux~$ echo "Hello World"
user@linux~$ echo $?
0
user@linux~$ bash nao_existe.sh
bash: ./nao_existe.sh: Arquivo ou diretório inexistente
user@linux~$ echo $?
1
```

## Variável `~`

O 'til' é semelhante à variável `HOME`. 

```bash
user@linux~$ echo ~
/home/user
```

É muito útil para ir para o diretório `HOME` quando você está em outro ponto do sistema de arquivos.

 
