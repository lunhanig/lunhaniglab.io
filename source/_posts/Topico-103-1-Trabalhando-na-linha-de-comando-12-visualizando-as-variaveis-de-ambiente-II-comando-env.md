---
title: Tópico 103.1 - Trabalhando na linha de comando 12 - visualizando as variáveis de ambiente (II)- comando env
date: 2020-08-06 09:37:14
tags:
  - export
  - variáveis
  - ambiente
  - global
  - env
---

Na postagem [anterior](/2020/07/24/Topico-103-Linha-de-comando-10-manipulando-variaveis-de-ambiente-III-exportando-variaveis/), foi possível visualizar variáveis locais e exportadas, em um `shell bash` com o comando `set`. Nesta postagem, veremos um outro comando, que permite visualizar apenas as variáveis globais.

<!-- more -->

## Comando `env`

O comando `env` é muito parecido com o `set`, se usado da maneira mais enxuta possível (isto é, digitando `env`):

| comando | funcionalidade                    |
| :------ | --------------------------------: |
| `set`   | mostra variáveis globais e locais |
| `env`   | mostra variáveis globais    |

Isso acontece porque o comando `env`, diferentemente do `set`, é um comando externo. Por ser externo, ele só enxergará as variáveis exportadas:

```bash
user@linux:~$ type set
set é um comando interno do shell
user@linux:~$ type env
env é /usr/bin/env
```

Na prática, se você definir uma variável local, ela só será visível para a sessão atual do `shell` e para o comando `set`. Para exemplificar, definirei uma variável `TESTE=Linux`,  e verificarei o que é apresentado por `set | less` e `env | less` (os resultados podem variar conforme o seu sistema): 


### `set | less`

```bash
user@linux:~$ TESTE=Linux
user@linux:~$ set | less
...
SHELL=/bin/bash
TERM=rxvt-unicode-256color
TESTE=Linux
TSBG1=100
:
```

### `env | less`

```bash
user@linux:~$ TESTE=Linux
user@linux:~$ env | less
...
SHELL=/bin/bash
TERM=rxvt-unicode-256color
TSBG1=100
```

Note que apenas com o comando `set` podemos visualizar a variável `TESTE`. Se quisermos que a variável apareça em `env`, teremos que executar antes, o comando `export TESTE=Linux` (ou `TESTE=Linux` e depois `export TESTE`):

```bash
user@linux:~$ export TESTE=Linux
...
SHELL=/bin/bash
TERM=rxvt-unicode-256color
TESTE=Linux
TSBG1=100
:
```

## Propósito do comando `env`

A funcionalidade do `env` vai além da visualização de variáveis. Sua real funcionalidade é executar programas com variáveis de ambiente modificadas, sem alterar o ambiente global.

Para isso, vamos retomar o seguinte *script*:

<script src="https://gist.github.com/lunhg/b17f2237125336068322571019069889.js?file=test_var_session.sh&__inline=true"></script>


Para executar este *script* corretamente, aprendemos que:

- podemos declarar uma variável local em uma sessão `shell` junto com a execução do *script*:

```bash
user@linux:~/scripts$ TESTE=Linux ./test_var_session.sh
Esse script testa a visibilidade de uma variável local
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo
TESTE=Linux
```

- podemos exportar a variável `TESTE` e depois executar o *script*:

```bash
user@linux:~/scripts$ export TESTE=Linux
user@linux:~/scripts$ ./test_var_session.sh
Esse script testa a visibilidade de uma variável local
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo
TESTE=Linux
```

Suponha que você já exportou a variável e quer mantê-la assim. Mas para propósitos de teste, que testar o *script* com outro valor para `TESTE`. É nesse sentido que o comando `env` será útil: ele alterará o valor da variável apenas na instância desejada.

```bash
user@linux:~/scripts$ echo $TESTE
Linux
user@linux:~/scripts$ env TESTE=Windows ./test_var_session.sh
Esse script testa a visibilidade de uma variável local
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo
TESTE=Windows
user@linux:~/scripts$ echo $TESTE
Linux
```
