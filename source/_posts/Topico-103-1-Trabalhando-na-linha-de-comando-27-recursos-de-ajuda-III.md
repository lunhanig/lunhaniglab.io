---
title: Tópico 103.1 - Trabalhando na linha de comando 27 - recursos de ajuda III
date: 2021-01-09 18:02:29
tags:
    - 103.1
    - ajuda
    - info
    - help
---

Nas postagens passadas vimos como usar o comando `man` (manual). Nesta postagem veremos um comando semelhante, o `info`.

 <!-- more -->

# Comando `info` 

O comando `info` é semelhante ao `man` em sua sintaxe:

| Comando | Opção       | Argumento     |
| :------ | :---------: | ------------: | 
| `info`  |             | `comando`     | 
| `info`  | `-k`        | `"proposição"`|
| `info`  | `--apropos` | `"proposição"`|

As diferenças do `info` para o `man` estão no formato de apresentação e no conteúdo. Seria possível dizer que o `info` é mais "simples" que o `man`. Pessoalmente, prefiro dizer que o `info` é menos "técnico" que o `man`, mas contêm uma explanação textual mais explicita e informacional.

