---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 2 - tac
date: 2021-01-18 13:02:08
tags:
    - 103.2
    - tac
    - filtros
---

A postagem anterior foi dedicada ao comando `cat`. Essa postagem é dedicada ao comando "inverso", `tac`

<!-- more -->

# Comando `tac`

Basicamente, o `tac` imprime na saída padão o mesmo conteúdo que o `cat` imprimiria, mas a partir da última linha para a primeira:

```bash
user@linux:~$ whatis tac
tac (1)       - concatenate files in reverse
user@linux:~$ tac alunos.txt
Bruno Rodrigo


André Batista
Roberto das flores

Paulo   Freire
Maria   Do Rosário
João Almeida

```

## Opções

Embora o comando `tac` seja o inverso do `cat`, ele não possue as mesmas opções do `cat`. Portanto, a opção `-b` terá um efeito diferente. Outras opções não estão disponíveis e talvez seja interessante você executar `tac --help`, `man tac` ou `info tac` para obter mais informações
