---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 5 - less
date: 2021-01-21 18:55:07
tags:
    - 103.2
    - less 
    - filtros
---

Com arquivos de texto muito grandes, se usarmos o comando `cat`, é provável que será necessário usar a barra de rolagem para lermos conteúdos no meio ou final do arquivo (supondo que estamos usando o terminal dentro de um ambiente gráfico). Mas em casos onde usamo o terminal fora do ambiente gráfico, não teremos a barra de rolagem disponível. É aí que o comando `less` pode ser útil.
<!-- more -->

# Comando `less`

Na prática, o `less` permite a paginação de um grande conteúdo textual. Mas de acordo com `info less` na seção DESCRIPTION:

```bash
user@linux:~$ info less
[... conteúdo ...]

DESCRIPTION

       Less is a program similar to more(1), but which allows backward movement in the file as well as forward movement.  Also, less does not have to read the entire input file before starting, so with large input files it starts up faster than text editors like vi(1).
```

Isto é:

> Less é um programa similar ao more(1), mas o qual permite movimento de retrocesso no arquivo bem como o movimento de avanço. O less também não tem que ler o arquivo de entrada antes de inicializar, então, para arquivos de entrada muito grandes, ele inicializa mais rápido que o vi(1).

## Navegando

| Tecla  | Função               |
| :----- | -------------------: |
| &#8593 | volta  em uma linha  |
| &#8595 | avança em uma linha  | 
| enter  | avança em uma linha  |
| espaço | avança em uma página |

## Procurando palavras


| Tecla                                   | Função                                  |
| :-------------------------------------- | --------------------------------------: |
| `/string` e depois enter                | procura pela palavra `string`           |
| `/string` e depois enter, e depois `n`  | procura a próxima aparição de `string`  |  
| `/string` e depois enter, e depois `N`  | procura a aparição anterior de `string` |

## Comandos úteis

| Tecla                 | Função                                    |
| :-------------------- | ----------------------------------------: |
| Ctrl-G                | mostra o status do arquivo                |
| g e depois um número  | vai até a linha especificada pelo número  | 
| d e depois um número  | desce N linhas especificadas pelo número  | 
| b e depois um número  | sobe  N linhas especificadas pelo número  |
| q                     | sai (_quit_) do less                      |
