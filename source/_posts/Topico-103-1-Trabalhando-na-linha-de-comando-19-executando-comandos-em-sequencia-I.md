---
title: Tópico 103.1 - Trabalhando na linha de comando 19 - executando comandos em sequência
date: 2020-12-16 20:30:52
tags:
  - shell
  - comandos
  - sequência
---

Iremos parar um pouco sobre o assunto de variáveis e adentrar um pouco algumas propriedades do `bash`. Mais especificamente sobre a sequencialidade de execução de comandos

<!-- more -->

Até o momento o `bash` foi usado da seguinte forma:

  * digitamos um comando
  * apertamos enter
  * digitamos outro comando
  * apertamos enter
  * digitamos ...

```bash
user@linux~$ echo Hello
Hello
user@linux~$ ls /tmp 
. ..
user@linux~$ echo $HOME
/home/user
user@linux~$ ...
```

Se observarmos o arquivos de histórico (`HISTFILE`), veremos que cada comando está em uma linha. No entanto é possível executarmos uma sequência de comandos em uma única linha. Podemos fazer isso de três formas diferentes. Essas formas se diferenciam pela maneira em que possíveis erros de execução são tratados.

# Ponto e vírgula `;`

O ponto e vírgula é usado quando queremos que os comandos em sequência sejam executados independentemente do erro encontrado. Isto é, mesmo se um erro em um comando ocorrer, o próximo será executado. 

Os comandos são separados por um `;`

## Exemplo 1: com erro

Neste exemplo, note que o diretório `/tmpt` não existe e o comando `ls /tmpt` retorna um erro. Mesmo assim o próximo comando é executado.

```bash
user@linux~$ ls /tmpt ; echo "Exemplo" ; echo 1 ; echo executado
/usr/bin/ls: não foi possível acessar '/tmpt': Arquivo ou diretório inexistente
Exemplo
1
executado
user@linux~$ 
```

## Exemplo 2: sem erro

Neste exemplo, note que o diretório `/tmp` existe e o comando `ls /tmp` é executado normalmente. Assim como o exemplo anterior, o próximo comando é executado. A única diferença é a saída impressa.

```bash
user@linux~$ ls /tmp ; echo "Exemplo" ; echo 1 ; echo executado
. .. 
Exemplo
1
executado
user@linux~$ 
```

# Duplo E comercial `&&`

O `&&` é usado quando queremos que um comando posterior apenas seja executado se o anterior não retornou algum erro: 

## Exemplo 3: com erro

Neste exemplo, note que o diretório `/tmpt` não existe e o comando `ls /tmpt` retorna um erro. O próximo comando não é executado.

```bash
user@linux~$ ls /tmpt && echo "Exemplo" && echo 1 && echo executado
/usr/bin/ls: não foi possível acessar '/tmpt': Arquivo ou diretório inexistente

user@linux~$ 
```

## Exemplo 4: sem erro

Neste exemplo, note que o diretório `/tmp` existe e o comando `ls /tmp` é executado normalmente. Diferente do exemplo anterior, o próximo comando é executado. 

```bash
user@linux~$ ls /tmp && echo "Exemplo" && echo 1 & echo executado
. .. 
Exemplo
1
executado
user@linux~$ 
```

# Duplo Pipe `||`


O `||` é usado quando queremos que um comando posterior apenas seja executado se o anterior retornou algum erro (é o inverso do `&&`)

## Exemplo 5: com erro

Neste exemplo, note que o diretório `/tmpt` não existe e o comando `ls /tmpt` retorna um erro. Desta forma, o segundo comando é executado, mas o terceiro e o quarto não:

```bash
user@linux~$ ls /tmpt || echo "Exemplo" || echo 1 || echo executado
/usr/bin/ls: não foi possível acessar '/tmpt': Arquivo ou diretório inexistente
Exemplo

user@linux~$ 
```

## Exemplo 6: sem erro

Neste exemplo, note que o diretório `/tmp` existe e o comando `ls /tmp` é executado normalmente. Diferente do exemplo anterior, nenhum comando subsequente é executado:

```bash
user@linux~$ ls /tmp || echo "Exemplo" || echo 1 || echo executado
. .. 

user@linux~$ 
```
