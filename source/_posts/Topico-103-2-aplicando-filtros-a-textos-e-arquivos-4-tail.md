---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 4 - tail
date: 2021-01-20 11:42:45
tags:
    - 103.2
    - tail
    - filtros
---

Assim como o `cat` possui o seu "inverso", o `head`também possui uma forma complementar, que é o comando `tail`.

<!-- more -->

# Comando `tail`

O `tail` imprime as linhas finais de um arquivo. Quando nenhuma opção de quantidade de linhas é dada, o `tail` imprime as 10 últimas linhas. 

Imagine que temos o conteúdo de `info tail` em um arquivo chamado `tail.txt`, localizado em `/home/user`.

```

File: coreutils.info,  Node: tail invocation,  Next: split invocation,  Prev: head invocation,  Up: Output of parts of files

5.2 ‘tail’: Output the last part of files
=========================================

‘tail’ prints the last part (10 lines by default) of each FILE; it reads
from standard input if no files are given or when given a FILE of ‘-’.
Synopsis:

     tail [OPTION]... [FILE]...

    [... mais conteúdo ... ]
```

Se executarmos o comando `tail tail.txt`, o `bash` nos mostrará, na saída-padrão, as 10 últimas linhas:

```bash
user@linux:~$ tail tail.txt

   Even if your script assumes the standard behavior, you should still
beware usages whose behaviors differ depending on the POSIX version.
For example, avoid ‘tail - main.c’, since it might be interpreted as
either ‘tail main.c’ or as ‘tail -- - main.c’; avoid ‘tail -c 4’, since
it might mean either ‘tail -c4’ or ‘tail -c 10 4’; and avoid ‘tail +4’,
since it might mean either ‘tail ./+4’ or ‘tail -n +4’.

   An exit status of zero indicates success, and a nonzero value
indicates failure.

user@linux:~$ 
```

## Opções comuns

As opções mais usadas para o `tail` são as mesmas que usamos com o `head`:

| Opção curta | Opção longa | Descrição                           |
| :---------- | :---------: | ----------------------------------: |
| `-n`        | `--lines`   | emite as primeiras linhas definidas |
| `-c`        | `--bytes`   | emite os primeiros bytes definidos  |


### Exemplo com linhas

```bash
user@linux:~$ tail -n 5 tail.txt 
since it might mean either ‘tail ./+4’ or ‘tail -n +4’.

   An exit status of zero indicates success, and a nonzero value
indicates failure.

user@linux:~$
```

### Exemplo com bytes

```bash
user@linux:~$ tail -c 5 tail.txt 
re.

user@linux:~$
```

## Opção diferente

Existe uma opção no `tail que não existe no `head`, `-f` ou `--follow`:

| Opção curta | Opção longa | Descrição                                                  |
| :---------- | :---------: | ---------------------------------------------------------: |
| `-f`        | `--follow`  | emite os dados anexados ao arquivo `amedida que ele cresce |

É uma opção muito útil quando executamos um comando que registra eventos em um arquivo `.log`, que serve para manter um histórico de como o comando funcionou, se possui erros, etc.

Suponha que temos o seguinte _script.sh_ que escreve indefinidamente (até que mandemos parar), a cada 1 segundo, a data em um arquivo _script.log_:

```bash
#!/usr/bin/env bash

while true; do 
    echo $(date) >> /home/user/script.log
    sleep 1
done
```

Se executarmos, em um terminal:

```bash
user@linux:~$ chmod +x script.sh
user@linux:~$ ./script.sh
```

E, em outro terminal, executarmos `tail -f script.log`, veremos que a cada 10 segundos a escrita do arquivo sendo executada. Note que o prompt do `bash` não aparece. Tal opção mantêm o arquivo `tai` "pendente" (fica aguardando entradas no arquivo e, quando escrito, mostrará na tela):

```bash
user@linux:~$ tail -f script.log 
qua 20 jan 2021 12:22:11 -03
qua 20 jan 2021 12:22:12 -03
qua 20 jan 2021 12:22:13 -03
qua 20 jan 2021 12:22:14 -03
qua 20 jan 2021 12:22:15 -03
```

Para "sair" do `tail -f`, aperte Ctrl-C.
