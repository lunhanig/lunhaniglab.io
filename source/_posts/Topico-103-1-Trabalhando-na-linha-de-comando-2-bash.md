---
title: Tópico 103.1 - Trabalhando na linha de comando 2 - bash
date: 2020-07-18 12:35:32
tags:
  - shell
  - sh
  - ash
  - bash
  - dash
  - csh
  - ksh
  - pdksh
  - mksh
  - zsh
  - csh
  - tcsh
  - fish
---

Como dissemos na [postagem anterior](/2020/07/18/Topico-103-Linha-de-comando-1-shell-date/), o shell é uma interface de usuário que usa uma linha de comando. No entanto existem vários tipos de `shell`, ou como chamam, *sabores* (flavour). Cada tipo de shell tem suas especificidades que incrementam funcionalidades ao primeiro `shell` Unix. Recomendo ver os seguinte [link](https://en.wikipedia.org/wiki/Unix_shell#Early_shells) para maiores informações.

<!-- more -->

## Sabores de Shell

De forma resumida, apresentamos uma tabela compilada com os *sabores de shell* que existiram e mais usados. Recomendo a leitura de cada tópico. Não cai na prova, mas é muito interessante observar, do ponto de vista histórico, o desenvolvimento dos sabores de shell:

| Nome abreviado | Nome completo                                                                           |
| :------------- | -------------------------------------------------------------------------------------:  |
| sh             | [Thompson Shell](https://en.wikipedia.org/wiki/Thompson_shell)                          |
| sh             | [PWB shell](https://en.wikipedia.org/wiki/PWB_shell)                                    |
| sh             | [Bourne Shell](https://en.wikipedia.org/wiki/Bourne_shell)                              |
| ash            | [Almquist Shell](https://pt.wikipedia.org/wiki/Almquist_shell)                          |
| bash           | [Bourne-Again Shell](https://en.wikipedia.org/wiki/Almquist_shell)                      |
| dash           | [Debian-Almiquist Shell](https://en.wikipedia.org/wiki/Debian_Almquist_shell)           |
| ksh            | [Korn Shell](https://en.wikipedia.org/wiki/Korn_shell)                                  |
| pdksh          | [Public Domain Korn Shell](https://en.wikipedia.org/wiki/Pdksh)                         |
| mksh           | [MirBSD Korn Shell](https://en.wikipedia.org/wiki/Mksh)                                 |
| zsh            | [Z-shell](https://en.wikipedia.org/wiki/Z_shell)                                        |
| csh            | [C-shell](https://en.wikipedia.org/wiki/C_shell)                                        |
| tcsh           | [TENEX C-shell](https://en.wikipedia.org/wiki/TENEX_C_shell)                            |
| fish           | [Friendly Interactive Shell](https://en.wikipedia.org/wiki/Friendly_interactive_shell)  |

## Qual é o shell exigido no exame

Não precisamos saber todos. O que é exigido no exame da LPIC é o `bash`, que é o `shell` escrito como parte do projeto `GNU` para prover um super-conjunto de funcionalidades do `Bourne Shell`. É o `shell` instalado por padrão na maioria das distribuições linux.
