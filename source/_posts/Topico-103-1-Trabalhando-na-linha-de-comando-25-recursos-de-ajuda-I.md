---
title: Tópico 103.1 - Trabalhando na linha de comando 25 - recursos de ajuda I
date: 2021-01-09 12:06:17
tags:
    - 103.1
    - ajuda 
    - man
    - help
---

 A partir de agora será estudado como acessar os tópicos de ajuda. Nesta postagem veremos um dos mais importantes: o comando `man` (manual).

 <!-- more -->

# Comando `man`

De forma geral, os comandos-padrão do do `shell` possuem um manual de referência. Cada um deles podem ser acessados pelo comando `man` e sua sintaxe é bem simples:

| Comando | Argumento |
| :------ | --------: |
| `man`   | `comando` |

## Exemplo do manual do comando `ls`

```bash
user@linux:~$ man ls
```

Quando digitamos isso, uma grande descrição substitui (visualmente) o _prompt_. Essa descrição possui um título (o comando em letras maiúsculas) o nome do comando (NAME), uma sinopse (SYNOPSIS) e uma descrição do comando e uso de opções e argumentos (DESCRIPTION):


```
                                User Commands
LS (1)

NAME
    ls - list directory contents

SYNOPSIS
    ls [OPTION]... [FILE]...

DESCRIPTION 
    List information about the FILEs (the current directory by default).  Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.

    Mandatory arguments to long options are mandatory for short options too.

    -a, --all
        do not ignore entries starting with .

    ...

Manual page ls(1) line 1 (press h for help or q to quit
```

## Navegando no manual

O manual é paginado e para navegar podemos apertar `seta para cima` ou `seta para baixo` para avançarmos na paginação.

Observer que, na parte inferior da tela, existe uma mensagem fixa `Manual page ls(1) line 1` que é modificada conforme avançamos na leitura do manual.

## Ajuda do manual

Essa mesma mensagem fixa indica que podemos pressionar `h` para obter mais ajuda a respeito do `man` (fundamentalmente, como usar o teclado para navegar no `man`). De fato, veremos que o `man` utiliza o `less` para visualizar cada manual:

```
            SUMMARY OF LESS COMMANDS

    Commands marked with * may be preceded by a number, N.
    Notes in parentheses indicate the behavior if N is given.
    A key preceded by a caret indicates the Ctrl key; thus ^K is ctrl-K.

h  H                Display this help.
q  :q  Q  :Q  ZZ    Exit.
-------------------------------------------------------------------------

                    MOVING

e  ^E  j  ^N  CR  * Forward one line  (or N lines).
...

HELP -- Press RETURN for more, o q when done
```

Observe que neste manual de ajuda do `man` existe outra mensagem fixa que indica o seguinte:

    - `Press RETURN for more`: aperte o "enter" para ver mais
    - `or q when done`: aperte o "q" para sair da ajuda do `man`

## Saindo do man

Da mesma forma que saímos da ajuda do `man` apertando o `q`, apertamos `q` para sair do `man`.

## Pesquisando no man

Podemos utilizar o padrão `/String` onde `/` indica que iremos procurar por um padrão de texto e `String` é a sequência de caracteres que estamos procurando. Para encontrar (depois de digitarmos) apertamos "enter".

## Comandos que não existem no man

De forma geral, apenas [comandos externos](/Topico-103-Linha-de-comando-4-o-que-e-um-comando-comando-type) possuem manuais de referência. A princípio, comandos externos não tem um manual (alguns como o `cd` podem ser referenciados por um manual POSIX). Se você quiser ver o manual dos comandos internos você precisa ver o manual do `bash` como o comando `man bash`:

Em algumas distribuições Linux, o seguinte pode ocorrer:

```bash
user@linux:~$ type cd 
cd é um comando interno do shell
user@linux:~$ man cd
no manual entry for `cd`
user@linux:~$ man bash
```

No entanto, o seguinte pode acontecer:

```bash
user@linux:~$ man cd


                            POSIX Programmer's Manual
CD(1P)

PROLOG
        This manual page is part os the POSIX Programmer's Manual.  The Linux implementation of this interface may differ (consult the corresponding Linux manual page for details of Linux behavior), or the interface may  not be implemented on Linux.

NAME
        cd - change the working directory
...
```

