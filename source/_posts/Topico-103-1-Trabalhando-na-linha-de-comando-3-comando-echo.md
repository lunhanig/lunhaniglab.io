---
title: Tópico 103.1 - Trabalhando na linha de comando 3 - comando echo
date: 2020-07-18 19:23:37
tags:
  - echo
  - variável
  - shell
  - bash
---

Na [postagem anterior](/2020/07/18/Topico-103-Linha-de-comando-2-bash/) comentei sobre o `shell` e seus tipos. Supondo que não sabemos qual tipo de `shell` estamos usando, como fazer para tomar conhecimento? 

Para isso precisamos do comando `echo`.

<!-- more -->

## Comando echo

O comando `echo` é uma palavra-chave utilizada para [apresentar um texto na tela do computador](https://pt.wikipedia.org/wiki/Echo_(comando)).

### Estrutura do comando echo

O comando `echo` necessita de um `argumento`:

| Comando | Argumento |
| :------ | --------: |
| echo    | Texto     |
| echo    | Variável  |

### Exemplo

Abaixo apresento exemplos de acordo com o primeiro item da tabela acima:

```bash
user@linux:~$ echo Linux
Linux
user@linux:~$ echo "Distro linux"
Distro Linux
```

Note que quando o texto for uma [cadeia de caracteres](https://pt.wikipedia.org/wiki/Cadeia_de_caracteres) que forma uma única palavra, não precisamos colocá-la entre áspas.

Caso contrário, se a cadeia de caracters formar mais de uma palavra separada por espaço, é necessário colocá-la entre áspas.

## Variáveis de ambiente

Em uma [postagem anterior](https://lunhanig.gitlab.io/2020/07/18/Topico-103-Linha-de-comando-1-shell-date/), comentei que o `shell` é um ambiente de programação. Como tal, é acompanhado por aquilo que se chama `variáveis de ambiente`:  [é um valor nomeado dinamicamente que pode afetar o modo como os processos em execução irão se comportar em um computador](https://pt.wikipedia.org/wiki/Vari%C3%A1vel_de_ambiente).

### Estrutura de uma variável de ambiente

Para capturarmosuma variável de ambiente, necessitamos preceder o caracter `$`:

`$` + `NOME_DA_VARIÁVEL` que resulta em `$NOME_DA_VARIÁVEL`

### Exemplos

| Variável | Represetação | o que memoriza                              |
| :------- | :----------: | :-----------------------------------------: |
| HOME     | `$HOME`      | o caminho para o diretório do usuário       |
| SHELL    | `$SHELL`     | o caminho para o tipo de shell sendo usado  |

## Recuperando o tipo de shell sendo usado

Na tabela sobre a estrutura do comando `echo`, comentamos que o arqumento pode ser uma variável. Para conhecermos qual tipo de `shell` estamos usando, precisamos executar o seguinte.

```bash
user@linux:~$ echo $SHELL
/bin/bash
```

O que isso quer dizer? Quer dizer que estamos usando o `bash` que está localizado no diretório `/bin`.
