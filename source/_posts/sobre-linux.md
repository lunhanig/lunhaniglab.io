---
title: Sobre certificações linux
date: 2020-07-16 14:33:53
tags: 
  - Linux 
  - LPIC-1
  - Certificações
---

A algum tempo comprei um ótimo [curso preparatório](https://www.udemy.com/course/curso-online-certificacao-linux-lpic1-comptia/) desenvolvido por Ricardo Prudenciato para a certificação [Linux Professional](https://www.lpi.org/our-certifications/lpic-1-overview). No entanto apenas fazia as lições e não tinha um meio de anotá-las. Este blog não substitui o curso e recomendo fortemente adquirirem. Adicionarei também algumas coisas a partir do que aprendi do livro ["Programação em Shell Linux"](https://books.google.com/books/about/Programa%C3%A7%C3%A3o_Shell_Linux_8%C2%AA_edi%C3%A7%C3%A3o.html?id=nHf5W51snDwC) do Professor Júlio César Neves e de antigos fóruns, como o [Viva o Linux](https://www.vivaolinux.com.br/).

<!-- more -->

## Por quê uso Linux?

Por que uso Linux? 

- Me estimulou a elaborar ferramentas musicais próprias;
- Existe uma comunidade ativa desenvolvendo áudio para linux 
- Posso otimizar ferramentas de áudio computacionalmente pesadas para computadores "fracos" (onde no windows, "engasgaria");

### A importância do linux

Esta lista são anotações do curso que comprei, algumas delas com adição de links confirmando as informações: 

- [É a base da infraestrutura da maioria das médias e grandes empresas](https://jornalwebdigital.blogspot.com/2017/06/pesquisa-revela-que-70-das-empresas-na.html)
- [Mais de 70% dos servidores web são Linux](https://canaltech.com.br/linux/Estudo-mostra-que-83-das-empresas-executam-Linux-em-seus-servidores/)
- [100% dos TOP 500 computadores são Linux](https://diolinux.com.br/2017/11/linux-em-100-dos-top-500-supercomputadores.html)
- [80% dos smarthphones são Linux - Android](https://olhardigital.com.br/noticia/site-lista-smartphones-capazes-de-rodar-o-linux/92911)
- [90% das aplicações em nuvenm são Linux](https://www.developer.com/daily_news/90-of-the-public-cloud-runs-on-linux.html)
- 30% dos *mainframes* são Linux
- 30% dos sistemas embarcados (aqueles que rodam em geladeiras, máquinas de lavar, etc.)

#### Importância da certificação

- Aumento das possibilidades de empregabilidade;
- Reconhecimento profissional;
- Aumento da autoconfiança (que é consequência da aquisição de conhecimento e reconhecimento);
- Estudo e aprendizagem (o mais importante não é o papel, é o processo de aprendizagem)
