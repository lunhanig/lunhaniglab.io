---
title: Tópico 103.1 - Trabalhando na linha de comando 30 - informação de sistema - uname
date: 2021-01-12 16:04:28
tags:
    - 103.1
    - uname
    - sistema
    - info
---

Nesta postagem veremos como obter informações específicas sobre o Sistema Operacional que estamos usando: `uname`. É um comando importante de conhecer e muito provavelmente cairá na prova.

<!-- more -->

# Comando `uname`

Agora que aprendemos a usar os comandos `man` e `info`, podemos aprender com o próprio sistema o que é o comando `uname`.

## Através do `man`

Ao digitarmos

```bash
user@linux:~$ man uname
```

Aparecerá a seguinte descrição: 

```bash
                        User Commands
UNAME(1)

NAME

    uname - print system information
```

ou

> uname - imprime informações de sistema.

Esta é uma descrição bastante sucinta. Mas podemos aprender mais com o comando `info`.

## Através do `info`

Ao digitarmos

```bash
user@linux:~$ info uname
```

Dentre várias informações, destacamos:

```bash
You can use the `uname` function to find out some information about the 
type of computer your program is running on. 

(...) `uname` also give some information identifying the 
particular system your program is running on. This is the same
information which you can get with functions targeted to this purpose (...)
```

> Você pode usar a função `uname` para encontrar alguma informação sobre o tipo de computador em que o seu programa está rodando. (...) o `uname` também dá alguma informação identificando o sistema particular que o seu programa está sendo executado. Esta é a mesma informação na qual você pode obter com funções direcionadas para este propósito (...)

## Exemplo básico

O comando `uname` possui diversas opções que especificam detalhes almejados. O uso mais trivial do `uname` pode ser pouco útil (uma vez que retorna apenas o valor `Linux`):

```bash
user@linux:~$ uname
Linux
```

## Obtendo ajuda para mais informações

Para sabermos quais informações são impressas pelo `uname`, tente executá-lo com a opção `--help`, que retornará as seguintes opções:

| Opção curta | Opção longa           | Descrição (emissão)                |
| :---------- | :-------------------: | ---------------------------------: |
| `-a`        | `--all`               | todas as informações (disponíveis) |
| `-s`        | `--kernel-name`       | o nome do Kernel                   |
| `-n`        | `--nodename`          | o nome do nó da máquina na rede    |
| `-r`        | `--kernel-release`    | a versão de lançamento do Kernel   |
| `-v`        | `--kernel-version`    | a data em que o Kernel foi criado  |
| `-m`        | `--machine`           | o nome do hardware da máquina      |
| `-p`        | `--processor`         | o tipo do processador              |
| `-i`        | `--hardware-platform` | a plataforma de hardware           |
| `-o`        | `--operating-system`  | o sistema operacional              |


### Informação completa (quando disponível):

Note que ambos os comandos retornam a mesma informação (note também que algumas informações, como `-p` e `i`, são omitidas, pois podem não estar disponíveis:

```bash
user@linux:~$ uname -a
Linux linux 5.6.7-mysys-1 #1 SMP PREEMPT Tue, 08 Dec 2020 12:34:56 +0000 x86_64 GNU/Linux
user@linux:~$ uname -snrvmo
Linux linux 5.6.7-mysys-1 #1 SMP PREEMPT Tue, 08 Dec 2020 12:34:56 +0000 x86_64 GNU/Linux
```

### Utilidade

É um comando muito útil quando você está desenvolvendo algum _script_ que será executado de acordo com uma máquina diferente. Veja este [exemplo](https://github.com/rootzoll/raspiblitz/blob/v1.6/build_sdcard.sh#L94). Ele verifica se a arquitetura da CPU é do tipo ARM (`arm` ou `aarch64`). Se for, a execução continua. Se não (`x86_64`), para de executar o _script_:

```bash
echo "Detect CPU architecture ..."
isARM=$(uname -m | grep -c 'arm')
isAARCH64=$(uname -m | grep -c 'aarch64')
isX86_64=$(uname -m | grep -c 'x86_64')
if [ ${isARM} -eq 0 ] && [ ${isAARCH64} -eq 0 ] && [ ${isX86_64} -eq 0 ] ; then
    echo "!!! FAIL !!!"
    echo "Can only build on ARM, aarch64, x86_64 or i386 not on:"
    uname -m
    exit 1
else
    echo "OK running on $(uname -m) architecture."
fi
```
