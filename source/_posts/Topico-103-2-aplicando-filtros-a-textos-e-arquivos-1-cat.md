---
title: Tópico 103.2 - Aplicando Filtros a textos e arquivos 1 - cat 
date: 2021-01-17 13:40:45
tags:
    - 103.2
    - cat
    - filtros
---

 partir desta postagem entramos no tópico 103.2 da LPI. Especificamente, nesta postagem veremos o comando `cat`.

<!-- more -->

# Comando `cat`

Se digitarmos `whatis cat` e `cat --help`, veremos qual a sua funcionalidade:

```bash
user@linux:~$ whatis cat
cat (1)       - concatenate files and print on the standard output
cat (1p)      - concatenate and print files
user@linux:~$ cat --help
Uso: cat [OPÇÃO]... [ARQUIVO]...
Concatena ARQUIVOS(S) para a saída padrão
...
```

Na prática, se inserirmos um argumento (arquivo) para o `cat`, veremos o conteúdo de um arquivo na saída padrão (a tela). Suponha um arquivo `alunos.txt`, cujo conteúdo é:

```
João
Maria
Carlos

Roberto
André

Bruno
```

Suponha também que este arquivo está localizado em `/home/user`. Se executarmos `cat alunos.txt`, teremos:


```bash
user@linux:~$ cat alunos.txt
João Almeida 
Maria   Do Rosário
Paulo       Freire

Roberto das flores
André batista

        
Bruno Rodrigo
```

## Opções interessantes do `cat`

| Opção curta | Opção longa        | Funcionalidade                                               |
| :---------- | :----------------: | -----------------------------------------------------------: |
| `-n`        | `--number`         | numera todas as linhas de saída                              |
| `-b`        | `--number-noblank` | numera linhas de saída não vazias                            |
| `-s`        | `--squeeze-blank`  | suprime linhas em branco repetidas                           |
| `-A`        | `--show-all`       | mostra todos os caracteres, incluindo "enters", "tabs", etc. |

### Opção `-n` ou `--number`

É interessante para ver o conteúdo, linha-a-linha, o comando `cat` com a opção `--number` ou `-n`. Ela imprime, do lado esquerdo, o número da linha, incluindo aquelas que estão em branco.

```bash
user@linux:~$ cat -n alunos.txt
    1 João Almeida 
    2 Maria   Do Rosário
    3 Paulo       Freire
    4
    5 Roberto das flores
    6 André batista
    7
    8        
    9 Bruno Rodrigo
```


### Opção `-b` ou `--number-noblank`

Para ver o conteúdo, linha-a-linha, excluindo aquelas que estão em branco, é interessante usar a opção `-b` ou `--number-noblank`:

```bash
user@linux:~$ cat -b alunos.txt
    1 João Almeida 
    2 Maria   Do Rosário
    3 Paulo       Freire
    
    4 Roberto das flores
    5 André batista
    
            
    6 Bruno Rodrigo
```


### Opção `-b` ou `--number-noblank`

No arquivo de texto, é possível ver que existe um espaço com duas linhas em branco. Pode ser útil suprimir linhas em branco repetidas e, para isso, podemos usar a opção `-s` ou `--squeeze-blank`. Note que, diferente de `cat alunos.txt` que imprime todas as linhas, apenas uma das linhas é suprimida:

```bash
user@linux:~$ cat -s alunos.txt
João Almeida 
Maria   Do Rosário
Paulo       Freire
    
Roberto das flores
André batista
    
Bruno Rodrigo

user@linux:~$ cat alunos.txt
João Almeida 
Maria   Do Rosário
Paulo       Freire
    
Roberto das flores
André batista
    

Bruno Rodrigo
```


### Opção `-A` ou `--show-all`

No arquivo de texto, podemos digitar "enter", "tab" e caracteres espciais como acetos e cedilha. Com o comando `cat alunos.txt`, o resultado textual observado está de acordo com o que queremos ler. 

No entanto, o computador codifica esses caracteres de outra forma; se quisermos ver o que de fato foi digitado, podemos usar a opção `-A` ou `--show-all`. Esta opção, de fato, é um "atalho" para o uso simultâneo de três opções:


| Opção curta | Opção longa          | Funcionalidade                                     |
| :---------- | :------------------: | -------------------------------------------------: |
| `-v`        | `--show-nonprinting` | usa a notação ^ e M- para acentos, cedilhas, etc.  |
| `-E`        | `--show-ends`        | exibe $ ao final de cada lina                      |
| `-T`        | `--show-tabs`        | exibe os caracteres de tabulação como ^I           |


```bash
user@linux:~$ cat -A alunos.txt
JoM-CM-#o Almeida$ 
Maria   Do RosM-CM-!rio$
Paulo       Freire$
$
Roberto das flores$
AndrM-CM-) Batista$
$
$
Bruno Rodrigo$
```

Note que o resultado é igual:


```bash
user@linux:~$ cat -vET alunos.txt
JoM-CM-#o Almeida$ 
Maria   Do RosM-CM-!rio$
Paulo       Freire$
$
Roberto das flores$
AndrM-CM-) Batista$
$
$
Bruno Rodrigo$
```
