---
title: Tópico 103.1 - Trabalhando na linha de comando - shell, date
date: 2020-07-18 09:16:59
tags:
  - shell
  - date
---

Esta postagem se refere ao conteúdo explicitado no [Tópico 103.1: trabalhando na linha de comando](https://learning.lpi.org/en/learning-materials/101-500/103/103.1/). 

<!-- more -->

## Peso 

O peso deste conteúdo para o exame 101 é de 4, ou seja, das 60 questões do exame, 4 questões serão sobre este assunto.

## Conhecimentos exigidos

De forma resumida, iremos aprender sobre:

- `shell`;
- comandos;
- linha de comando;
- ambiente;
- variáveis de ambiente;
- histórico

Mais especificamente, segundo o [Tópico 103.1: trabalhando na linha de comando](https://learning.lpi.org/en/learning-materials/101-500/103/103.1/), iremos: 
 
- Usar comandos simples de `shell` e sequências de comando em uma linha para executar tarefas básicas na linha de comando;
- Usar e modificar o ambiente `shell`, incluindo como definir, referenciar e exportar variáveis de ambiente;
- Usar e editar o histórico de comandos;
- Invocar comandos dentro e fora de um `PATH` definido.

## O `shell`

O `shell` é uma interface de usuário. Uma interface de usuário é um espaço apropriado para interação entre um humano e a máquina. No nosso caso, é o espaço apropriado para interagirmos com um sistema operacional do tipo `GNU/Linux`, afim de termos acesso aos recursos que este último administra.

Mais especificamente, iremos usar o `shell` através de uma *interface de linha de comando* (CLI).

![Figura 1: shell acessado através de uma linha de comando](https://upload.wikimedia.org/wikipedia/commons/2/29/Linux_command-line._Bash._GNOME_Terminal._screenshot.png)

### Quando acessamos um shell?

Depende de como sua máquina está configurada e da forma que você está acessando esta máquina:

#### Em desktops comuns

Em desktops comuns, como ubuntu, acessamos primeiro uma interface gráfica. Esta interface gráfica pode conter um aplicativo que emular um `shell` através de uma linha de comando. Existem muitos, como *xterm*, *konsole*, *urxvt*, etc...

![Figura 2: xterm](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Xterm_45.png/250px-Xterm_45.png)

#### Em servidores ou instaladores

Pode ser que um servidor esteja configurado para não ter interface gráfica. Desta forma, quando você ligar a máquina e esta for "carregada completamente", você irá acessar um `shell`.

![Figura 3: Acessandoo um shell sem interface gráfica](https://miloserdov.org/wp-content/uploads/2018/08/53-768x576.jpg)

### Shell como ambiente

O `shell` pode ser entendido como um *ambiente de programação*. Isto é, podemos executar e definir procedimentos automatizados. Ele pode ser usado "ao vivo" (livecoding) ou estruturado através de `scripts`, que são uma sequência de comandos que definem um procedimento maior e estruturado.

### Estrutura do shell

A adentrarmos no `shell`, podemos ver o seguinte:

```bash
user@linux:~$
```

Os caracteres `user@linux:~$` significa que adentramos em um [`prompt`](https://pt.wikipedia.org/wiki/Interface_de_linha_de_comandos).

Os caracteres concatenados `user@linux` significa que estamos logado em uma máquina `linux` (isso pode ser modificado durante uma instalação de sistema) e que estamos *logados* em uma sessão na máquina `linux` através do usuário `user`.

Os caracteres `:~$` significa que acessamos um `shell` e adentramos o *sistema de arquivos* (veremos mais sobre isso no futuro). O caracter `~` é uma abreviação para um diretório específico do sistema de arquivos: o *home do usuário*. Isto é, o diretório específico do usuário `user`. `~` é o mesmo que `/home/user`. E finalmente, o caracter `$` é o próprio shell.

Após todos esses caracteres, você pode ver um cursor piscando ou destacando. Este é o espaço para digitarmos comandos.

#### Executando um comando e vendo seu resultado

O primeiro comando que iremos aprender é o comando `date`, que retorna o ano, mês, dia, hora, minutos e segundos computados por nosso sistema. O resultado do comando irá aparecer na linha de baixo. Dependendo do sistema que você usa, o resultado pode sofre variações. No meu caso é assim:

```bash
user@linux:~$ date
sáb 18 jul 2020 09:16:56 -03 
```

Na próxima postagem iremos ver os tipos de `shell` e mais alguns comandos.


