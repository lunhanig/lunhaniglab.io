---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 3 - head
date: 2021-01-19 19:23:07
tags:
    - 103.2
    - head
    - filtros
---

Nas postagens anteriores falei sobre o `cat` seu inverso, o `tac`. Nesta postagem veremos sobre outro comando que também imprime o conteúdo de arquivos -- mas só o começo deles -- o `head`.

<!-- more -->

# Comando `head`

O `head` só imprime o cabeçalho do arquivo, isto é, o começo de cada arquivo. Quando nenhuma opção de quantidade de linhas é dada, o `head` imprime as 10 primeiras linhas. 

Imagine que temos o conteúdo de `info head` em um arquivo chamado `head.txt`, localizado em `/home/user`.

```
File: coreutils.info  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files
5.1 ‘Head’: Output the first part of files
==========================================

‘head’ prints the first part (10 lines by default) of each FILE; it
reads from standard input if no files are given or when given a FILE of
‘-’.  Synopsis:

    head [OPTION]... [FILE]...

    If more than one FILE is specified, ‘head’ prints a one-line header 
consisting of:

    ==> FILE NAME <==

before the output for each FILE.

The program accepts the following options.  Also see *note Common options::.

‘-c [-]NUM’
‘--bytes=[-]NUM’
Print the first NUM bytes, instead of initial lines.  However, if
NUM is prefixed with a ‘-’, print all but the last NUM bytes of
each file.  NUM may be, or may be an integer optionally followed

[... mais conteúdo ...]
```

O arquivo acima tem mais de 10 linhas. Se digitarmos, no terminal, `head head.txt`, teremos, na saída-padrão, apenas as 10 primeiras linhas:

```bash
user@linux:~$ head head.txt
File: coreutils.info,  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files

5.1 ‘head’: Output the first part of files
==========================================

‘head’ prints the first part (10 lines by default) of each FILE; it
reads from standard input if no files are given or when given a FILE of
‘-’.  Synopsis:

     head [OPTION]... [FILE]...

user@linux:~$ 
```

## Opções comuns

| Opção curta | Opção longa | Descrição                           |
| :---------- | :---------: | ----------------------------------: |
| `-n`        | `--lines`   | emite as primeiras linhas definidas |
| `-c`        | `--bytes`   | emite os primeiros bytes definidos  |

### Linhas

Ao invés de pedirmos para imprimir as 10 linhas por padrão, podemos imprimir, por exemplo, as quatro primeiras linhas:


```bash
user@linux:~$ head -n 4 head.txt
File: coreutils.info,  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files

5.1 ‘head’: Output the first part of files
==========================================
user@linux:~$ 
```

Atente que podemos colocar o número concatenado com `-n` de outra forma e teremos o mesmo resultado:

```bash
user@linux:~$ head -n4 head.txt
File: coreutils.info,  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files

5.1 ‘head’: Output the first part of files
==========================================

user@linux:~$ 
```

Se usarmos a opção longa, podemos usar de duas formas:


```bash
user@linux:~$ head --lines 4 head.txt
File: coreutils.info,  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files

5.1 ‘head’: Output the first part of files
==========================================

user@linux:~$ head --lines=4 head.txt
File: coreutils.info,  Node: head invocation,  Next: tail invocation,  Up: Output of parts of files

5.1 ‘head’: Output the first part of files
==========================================

user@linux:~$ 
```

### Bytes

Ao invés de pedirmos para imprimir as primeiras linhas, podemos imprimir, por exemplo, os quatro primeiros bytes do arquivo. Na maioria das vezes, um byte corresponde a um caractere, mas pode ocorrer de um caractere especial (principalmente em línguas não anglófonas) usar mais de um byte:


```bash
user@linux:~$ head -c 4 head.txt
File
user@linux:~$
```

O que descrevemos, na subseção anterior, das variantes das formas de digitar o comando também se aplicam aqui. Isto é, `-c 4` equivale a `-c4`, `--bytes 4` e `--bytes=4`.

Para mais informações, sugerimos a leitura de `info head` e `man head`.
