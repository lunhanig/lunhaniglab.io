---
title: Tópico 103.1 - Trabalhando na linha de comando 11 - visualizando as variáveis de ambiente (I) - comando set
date: 2020-08-05 11:14:17
tags:
  - export
  - variáveis
  - ambiente
  - global
  - set
  - pipe
  - less
---

Na postagem [anterior](/2020/07/24/Topico-103-Linha-de-comando-10-manipulando-variaveis-de-ambiente-III-exportando-variaveis/), foi possível ver como exportar variáveis locais para variáveis globais.

Nesta postagem iremos ver como visualizar variáveis locais e exportadas, em um `shell bash`.

<!-- more -->

## Comando `set`

Para visualizarmos variáveis de ambiente, sejam elas definidas localmente ou globalmente, na sessão atual do `shell`, nós podemos usar o comando `set`. O comando `set` é um comando interno (lembra-se do comand `type`?) que é usado para definir valores das variáveis no sistema. Ao usarmos o comando sem nenhuma opção ou argumento, ele nos retorna todas variáveis globais e locais definidas na sessão atual.

Eis um exemplo: digite `set` no seu `shell` e note que a o terminal imprimi um conteúdo extenso.

```bash
user@linux:$~ set
...
...
...
...
```

### Paginando o conteúdo retornado pelo `set`

Dependendo da configuração do seu sistema, a quantidade de variáveis definidas é enorme. Para [paginarmos](https://pt.wikipedia.org/wiki/Mem%C3%B3ria_paginada) o conteúdo, podemos usar dois comandos em conjunto com o `set`. Esses comando são o `|` (*pipe*) e o `less`. Não é necessário se preocupar com o significado deles agora. Veremos seus funcionamentos em um momento oportuno:

| comando | significado                                                 |
| :------ | ----------------------------------------------------------: |
| `set`   | imprimi as variáveis                                        |
| `|`     | direciona a saído do `set` para outro comando               |
| `less`  | executa uma paginação a partir do conteúdo passado pelo `|` |

Uma saída de um sistema operacional meu é assim:

```bash
user@linux:$~ set | less
BASH=/usr/bin/bash
BASHOPTS=checkwinsize:cmdhist:complete_fullquote:expand_aliases:extglob:extquote:force_fignore:globasciiranges:interactive_comments:progcomp:promptvars:sourcepath
BASH_ALIASES=()
BASH_ARGC=([0]="0")
BASH_ARGV=()
BASH_CMDS=()
BASH_COMPLETION_VERSINFO=([0]="2" [1]="10")
BASH_LINENO=()
BASH_REMATCH=()
BASH_SOURCE=()
BASH_VERSINFO=([0]="5" [1]="0" [2]="17" [3]="1" [4]="release" [5]="x86_64-pc-linux-gnu")
BASH_VERSION='5.0.17(1)-release'
:
```

Note o `:` ao final. Ele significa que está esperando um comando interno do `less`: apertanto para baixo, veremos mais variáveis (como se estivessemos passando a página de um livro). Apertando para cima, voltamos a ver variáveis que foram apresentadas primeiro. Apertando `h` ver um manual de ajuda com os comandos de ajuda do `less` e apertando `q`, nós iremos sair do `less` e voltar para o `bash`.
