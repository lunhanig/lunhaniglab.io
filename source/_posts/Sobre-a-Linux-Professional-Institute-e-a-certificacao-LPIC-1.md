---
title: Sobre a Linux Professional Institute e a certificação LPIC-1
date: 2020-07-16 18:59:41
tags:
  - Linux
  - LPIC-1
  - Certificações
---

A *Linux Professional Institute*, ou [LPI](https://www.lpi.org/our-certifications/lpic-1-overview) é uma instituição sem fins lucrativos. Ela busca promover:

- O uso de tecnologias livres;
- Elevar as pessoas que usam tecnologias livres;
- Promover programas de certificações em sistemas `GNU/Linux` e *open source*, independente de suas distribuições.

<!-- more -->

## Certificações

Certificação educacional:

- [Linux Essentials](https://www.lpi.org/our-certifications/linux-essentials-overview): para propósitos educacionais, para quem quer ter experiência com linux. Mas profissionalmente não tem tanta importância.

Certificações Profissionais (a Linux Essentials não é pré-requesito):

- [LPIC-1](https://www.lpi.org/lpic-1-linux-certified-administrator)
- [LPIC-2](https://www.lpi.org/lpic-2-linux-certified-engineer)
- LPIC-3 é dividida em 3 especializações:
  - [LPIC-3 300](https://www.lpi.org/lpic-3-mixed-environments)
  - [LPIC-3 303](https://www.lpi.org/lpic-3-security)
  - [LPIC-3 304](https://www.lpi.org/lpic-3-virtualization-and-high-availability)
- [DevOps](https://www.lpi.org/our-certifications/devops-overview)
- [BSD](https://www.lpi.org/our-certifications/bsd-overview)

## Para quem é voltada estas certificações?

Essas certificações são voltadas para administradores de sistemas (eu sou músico de formação, mas gosto muito de Linux e resolvi me aprofundar):

- O profissional deve ser capaz de realizar tarefas manutenção, instalação e configuração em qualquer sistema linux.

### Aprovação para certificação LPIC-1:

Para obter a certificação LPIC-1 é necessário passar em duas provas:

- 101-500 (500 significa versão 5.0);
- 101-500.

Esse certificado tem validade de 5 anos

#### Quantidade de questões e duração da prova

Cada prova tem 60 questões de duração 1:30 hora cada, na língua desejada.

#### O que preciso saber para passar?

Segundo a [LPI](https://www.lpi.org/our-certifications/lpic-1-overview), é necessário:

- Entender a arquitetura de um sistema Linux;
- Instalar e manter um ambiente de trabalho Linux, incluindo um X11 e configurá-lo como um cliente de rede;
- Trabahar na linha de comando, incluindo comandos GNU e Unix comumente usados;
- Lidar com permissões de acesso aos arquivos, bem como segurança do sistema; e
- Realizar tarefas de manutenção: auxiliar usuários, adicionar usuários a um sistema mair, *backup* e restauração, desligar e *reboot*.

#### Tópicos dos exames?

Lembre-se que cada tópico tem subtópicos. 

##### Exame 101-500

- 101: Arquitetura de sistema
- 102: Instalação do Linux e Gerenciamento de Pacotes;
- 103: Comandos GNU e Unix (Será o início da aprendizagem, como pré-requisito para sabermos os outros tópicos);
- 104: Dispositivos, Sistemas de Arquivos e FHS.

É aconselhável que, assim que terminar de estudar estes tópicos, você realize a prova 101-500.

##### Exame 102-500

- 105: Shells e *shell scripting*
- 106: Interfaces de usuário e desktops;
- 107: Tarefas administrativas;
- 108: Serviços essenciais do sistema;
- 109: Fundamentos de redes
- 110: Segurança

#### Agendamento e realização dos exames

Para realizar a prova, é necessário agendar com a [PearsonVUE](https://www.lpi.org/exam-pricing), e ir a um centro de teste mais próximo para realização do teste. Existe uma nota referente ao [distanciamento social](https://home.pearsonvue.com/coronavirus-update).
