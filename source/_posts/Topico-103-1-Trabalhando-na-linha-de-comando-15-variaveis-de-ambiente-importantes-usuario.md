---
title: Tópico 103.1 - Trabalhando na linha de comando (15) - variáveis de ambiente importantes II - usuário
date: 2020-12-13 19:25:35
tags:
  - variáveis
  - ambiente
  - global
  - usuário
---

Na última postagem, aprendemos um pouco sobre como são memorizados os comandos de um shell em um arquivo de histórico, bem como o processo de memorização por tamanho deste arquivo. Nesta postagem vamos ver um pouco sobre variáveis de ambiente que dizem respeito aos usuários.

<!-- more -->

## Variável `HOME`

A variável`HOME` mostra qual é a "casa" do usuário logado no momento, isto é, a pasta específica de um usuário na árvore de diretórios UNIX, a partir de uma pasta chamada `/home`. Por exemplo, se você logar em um Linux com o usuário `user`, o `HOME` do usuário `user` será `/home/user`:

```bash
user@linux~$ echo $HOME
/home/user
```

# Variável `HOSTNAME`

Se traduzirmos literalmente este termo, teremos algo como "nome do anfitrião". 


```bash
user@linux$ echo $HOSTNAME
linux
```

Em outras palavras, o anfitrião é a máquina que você está usando/acessado através de um usuário. Esta variável é definida quando instalamos o Sistema Operacional em uma máquina. É muito útil quando lidamos com redes de computadores.

# Variável `LOGNAME`

Esta variável memoriza o nome do usuário que fez o login na sessão atual.

```bash
user@linux~$ echo $LOGNAME
user
```

# Variável `USER`

Em um primeiro momento, esta variável é igual à variável `LOGNAME`. 

```bash
user@linux~$ echo $LOGNAME
user
user@linux~$ echo $USER
user
```

Mas, segundo [este artigo](https://knowledge.broadcom.com/external/article/37575/user-vs-logname-environment-variables.html), podem existir diferenças. Considere o seguinte: se você logar em uma sessão `shell` como `root` e executar os mesmos comandos acima, eles retornarão `root`:

```bash
root@linux~$ echo $LOGNAME
root
root@linux~$ echo $USER
root
```

Agora suponha que, na mesma sessão `shell`, você muda de usuário (você pode fazer isso através de um comando que iremos aprender, o `su` -- *muito cuidado ao usá-lo*):


```bash
root@linux~$ su user -
user@linux~$ echo $LOGNAME
root
user@linux~$ echo $USER
user
```


# ATENÇÃO

Geralmente podemos ver, no prompt da linha de comando,  o *usuário* concatenado com um *@* e o  *hostname*, formando uma `string` na forma `LOGNAME@HOSTNAME`

```bash
#usuario `user` na máquina `linux` pronto para user o shell
user@linux~$ echo $LOGNAME@$HOSTNAME
user@linux
```
