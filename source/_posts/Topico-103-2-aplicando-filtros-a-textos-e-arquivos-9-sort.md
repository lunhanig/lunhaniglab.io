---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 9 - sort
date: 2021-01-30 18:30:16
tags:
    - 103.2
    - sort
    - ordenação
---

Outra função importante, além da contagem de linhas em um arquivo de texto ou entrada-padrão, é a função de ordenar conteúdos alfabeticamente. Para isso usamos o comando `sort`.

<!-- more -->

# Ordenando com o comando `sort`

O comando `sort`, segundo o sua própria descrição (`sort --help`):

> Escreve de forma ordenada a concatenação do(s) ARQUIVO(S) na saída padrão.

Isto é, supondo que se, com o comando `cat alunos.txt` temos:

```bash
user@linux:~$ cat alunos.txt
João Almeida 
Maria   Do Rosário
Paulo       Freire
    
Roberto das flores
André batista
    
            
Bruno Rodrigo
```

com o comando `sort alunos.txt`, a saída fica:

```bash
user@linux:~$ sort alunos.txt


André batista
Bruno Rodrigo
João Almeida
Maria   Do Rosário
Paulo       Freire
Roberto das flores
```

Note que as duas primeiras linhas estão em branco; isso ocorreu porque o o comando `sort` dá precdência às linhas em branco:


```bash
user@linux:~$ sort alunos.txt | cat -n	
     1	
     2	
     3	André batista
     4	Bruno Rodrigo
     5	João Almeida
     6	Maria   Do Rosário
     7	Paulo       Freire
     8	Roberto das flores
```
