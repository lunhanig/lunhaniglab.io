---
title: Tópico 103.1 - Trabalhando na linha de comando 21 - navegando no histórico II
date: 2020-12-24 20:41:34
tags:
  - shell
  - comandos
  - history
---

Nessa postagem daremos continuidade ao assunto de propriedades do histórico. Na postagem anterior foi possível aprender que, através das setas "para cima" e "para baixo", é possível procurar comando que foram executados anteriormente. Aprenderemos agora o comando `history`

<!-- more -->

# Comando  `history`

Se executarmos o comando `history` da maneira mais simples, ele apresenta o conteúdo do arquivo de hisórico definido por `HISTFILE` junto com uma numeração indicativa da sequência histórica.

```bash
user@linux~$ history
  ...
  ...
  ...
  544  history --help
  545  set | grep HISTFILE
  546  history
```

Os números 544, 545 e 546 mostram que o comando, ao lado direito do número é indexado como os 544<sup>o</sup>, 545<sup>o</sup> e 546<sup>o</sup> comandos executados (de acordo com as regras definidas pelas variável `HISTFILESIZE`).

# Limpando o histórico

Existem duas formas de limpar o histórico: deletando o arquivo definido por `HISTFILE` ou executar o comando `history` com um arqgumento de limpeza. Talvez seja mais seguro executar a segunda opção (que oferece duas formas diferentes de fazer a mesma coisa):

```bash 
user@linux~$ history clear
```

Ou

```bash 
user@linux~$ history -c 
```

Ambos os comando fazem a mesma coisa.

## Aprendendo um pouco mais sobre o `history`

Sugiro executar o comando `history` com a opção `help` para mais informações:


```bash 
user@linux~$ history --help
```


