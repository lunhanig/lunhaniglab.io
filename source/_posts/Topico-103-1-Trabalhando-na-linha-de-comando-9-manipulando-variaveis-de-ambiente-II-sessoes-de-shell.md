---
title: Tópico 103.1 - Trabalhando na linha de comando 9 - manipulando variáveis de ambiente (II) - entendendo melhor a variável local e sessões de `shell`
date: 2020-07-23 14:20:18
tags:
  - variável
  - ambiente
  - local
  - global
  - bash
  - exit
---

Na postagem [anterior](/2020/07/23/Topico-103-Linha-de-comando-8-manipular-variaveis-de-ambiente-I) vimos como criar uma variável. Esta variável criada existe *localmente* na sessão `shell` que estamos usando. A presente postagem esclarecerá o significado disso e comentará sobre como *exportar* essa variável para outras sessões de `shell`.

<!-- more -->

## Sessão `shell`

Descrevi anteriormente o conceito de `shell` ([aqui](/2020/07/18/Topico-103-Linha-de-comando-1-shell-date/#O-shell)). Alí vimos que o `shell` serve como uma interfacede usuário para interagir com o sistema operacional. 

O termo "sessão" utilizado vem da palavra "session", em inglês. Se compararmos um dos significados da [primeira](https://dicionario.priberam.org/sess%C3%A3o) com a [segunda](https://translate.google.com/#view=home&op=translate&sl=auto&tl=pt&text=session) palavra, teremos:

### Priberam

> Sessão: Tempo durante o qual um corpo deliberativo está reunido em assembleia

### Google Translate

> Session: um período devotado para uma atividade particular.

### LPI

Ademais, podemos tomar o conceito de sessão de `shell` extraído da [LPI](https://learning.lpi.org/pt/learning-materials/010-160/2/2.1/2.1_02/):

> Todos os shells gerenciam um conjunto de informações de status ao longo das sessões do shell. Essas informações de tempo de execução podem mudar durante a sessão e influenciar o comportamento do shell. Esses dados também são usados pelos programas para determinar aspectos da configuração do sistema. A maioria desses dados é armazenada nas chamadas variáveis, que abordaremos nesta lição.) 

### Juntando os significados

Desses significados podemos extrair a seguinte ideia: uma sessão de `shell` dura um determinado tempo para a execução de determinadas tarefas. Nesse tempo, uma sessão de `shell` poderá armazenar informações específicas.

Uma consequência desta ideia é de que duas sessões diferentes irão compartilhar de informações armazenadas nas variáveis *globais**, mas não compartilharão as variáveis *locais*.

### Exemplo 1

Abra dois emuladores de terminal ao mesmo tempo. Cada um terá a sua própria sessão de `shell`. Após isso execute os seguintes comandos:

| Sessão 1                            | Sessão 2                             |
| :---------------------------------- | -----------------------------------: |
| `user@linux:~$ VARIAVEL_UM=valor1`  | `user@linux:~$ VARIAVEL_DOIS=valor2` |
| `user@linux:~$ echo $VARIAVEL_UM`   | `user@linux:~$ echo $VARIAVEL_DOIS`  |
| `valor1`                            | `valor2`                             |
| `user@linux:~$ echo $VARIAVEL_DOIS` | `user@linux:~$ echo $VARIAVEL_UM`    |
| `            `                      | `            `                       |

Você notará que quando pedimos pra imprimir (comando `echo`) o valor de uma variável definida em outra sessão de `shell`, o valor retornado é *vazio*.

### Exemplo 2

Outra maneira de demostrar isso é criando uma nova sessão de `shell` dentro do `shell` que estamos usando. Para isso usaremos os comandos `bash` (para criar uma nova sessão) e `exit` (para sair da sessão criada):

```bash
user@linux:~$ MINHA_VARIAVEL="Isso é um teste de sessão"
user@linux:~$ echo $MINHA_VARIAVEL
Isso é um teste de sessão
user@linux:~$ bash
user@linux:~$ echo $MINHA_VARIAVEL

user@linux:~$ exit
user@linux:~$ echo $MINHA_VARIAVEL
Isso é um teste de sessão
```

### Exemplo 3

Uma maneira diferente de demonstrar a localidade de uma variável e sessões de `shell` é através do uso de *scripts*. Realize o download do [*script*](https://gist.github.com/lunhg/b17f2237125336068322571019069889) abaixo no diretório `/home/user/scripts`:

<script src="https://gist.github.com/lunhg/b17f2237125336068322571019069889.js?file=test_var_session.sh&__inline=true"></script>

Defina a variável `TESTE`:

```bash
user@linux:~$ TESTE=testado
user@linux:~$
```

Agora navegue até o diretório `/home/user/scripts` (comando `cd`), verifique se o arquivo está presente no diretório (comando `ls`) e atribua uma permissão de execução para o arquivo baixado (comando `chmod`)

```bash
user@linux:~$ cd scripts
user@linux:~/scripts$ ls
.
..
echo_test.sh
test_var_session.sh
user@linux:~/scripts chmod +x test_var_session.sh
user@linux:~/scripts
```

Agora execute o *script*:

```bash
user@linux:~/scripts ./test_var_session.sh
Esse script testa a visibilidade de uma variável local. 
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo:
TESTE=
```

Note que a variável não apareceu. Isso acontece pois assim que o *script* é executado, uma nova sessão de `shell` é invocada. Esta por sua vez executa o *script* e finaliza a sessão, retornando para a sessão da qual executamos o *script*. Uma maneira de contornar isso é definindo a variável junto com a invocação do *script*. Com isso atribuímos um valor para a variável `TESTE` para a sessão de `shell` que é invocada:

```bash
user@linux:~/scripts TESTE=testado ./test_var_session.sh
Esse script testa a visibilidade de uma variável local. 
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo:
TESTE=testado
```

Na próxima postagem mostrarei como executar o *script* sem a necessidade de declarar a variável repetidas vezes.
