---
title: Tópico 103.1 - Trabalhando na linha de comando 33 - quoting
date: 2021-01-15 21:50:04
tags:
    - 103.1
    - aspas
    - apóstrofo
    - barra invertida
---

Até o momento foram vistos diversos comandos e variáveis de ambiente, onde alguns caracteres possuem uma função bem determinada, o que impede usá-los como eles são literalmente. Nesta postagem falarei sobre o _quoting_, ou como usar `""`, `''` e `\` em comandos.

<!-- more -->

# Quoting

Segundo o manual do bash (`man bash` ou [ver manual online](https://www.gnu.org/software/bash/manual/html_node/Quoting.html)):

>Quoting é usado para remover o significado especial de certos caracteres ou palavras do shell. Quoting pode ser usado para desabilitar o tratamento especial dado para caracters especiais, prevenindo certas palavras de serem reconhecidas como tais, e prevenindo expansão de parâmetros.

Segundo o manual existem trẽs mecanismos de _quoting_: o caractere de escape (a barra invertida `\`), aspas simples (`''`) e aspas duplas (`""`).:

## Caractere de escape

> A barra invertida é um caracter de escape. *Ele preserva o valor literal do próximo caractere que se segue, com exceção de <newline>*


Imagine que definimos uma variável `TESTE=Blog`. Note a diferença quando não precedemos uma barra invertida no caractere `$` e quando precedemos uma barra invertida no caractere `$`:

```bash
user@linux:~$ echo $TESTE
Blog
user@linux:~$ echo \$TESTE
TESTE
```

## Aspas simples

>Envolver caracteres em aspas simples preserva o valor literal de todos os caracteres dentro das aspas

Note que o `$TESTE` dentro de aspas, o caractere `$` será interpretado como literal:

```bash
user@linux:~$ echo '$TESTE'
$TESTE
```

## Aspas duplas

>Envolver caracteres em aspas duplas preserva o valor literal de todos caracteres dentro das aspas, com exceção de $, ` e `

Note que agora podemos user `$TESTE` como um valor dentro das aspas:


```bash
user@linux:~$ echo "$TESTE"
Blog
user@linux:~$ echo "\$TESTE"
$TESTE
user@linux:~$ echo "\$TESTE=$TESTE"
$TESTE=Blog
user@linux:~$ echo "$TESTE"
Blog=Blog
```

Sugiro uma leitura atenta da seção `Quoting` em `man bash`.
