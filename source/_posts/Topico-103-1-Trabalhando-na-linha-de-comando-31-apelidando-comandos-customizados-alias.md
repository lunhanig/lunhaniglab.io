---
title: Tópico 103.1 - Trabalhando na linha de comando 31 - apelidando comandos customizados - alias
date: 2021-01-13 18:36:57
tags:
    - 103.1
    - alias
    - customização
    - comando
---

Nesta postagem veremos como customizar comandos através do: `alias`.

<!-- more -->

# Comando `alias`

O `alias` permite que você visualize e defina comandos customizados através de um "apelido". 

## Redefinindo comandos existentes

Por exemplo, é muito comum usarmos o o comando `ls` repetidas vezes. Mas, em sua forma padrão, ele não oferece as informações que repetidamente buscamos (como as permissões de arquivos, tamanho, etc.) e seja muito repetitivo executá-lo com opções que repetidamente queremos. Podemos redefinir o `ls`, recursivamente, como:

    - sempre mostrando as permissões de arquivos: `ls -l`
    - sempre mostrando o tamanho de cada arquivo: `ls -s`
    - sempre mostrando arquivos ocultos: `ls -a`
    - ou tudo isso junto: `ls -lsa`

```bash 
user@linux:~$ ls /home
user
user@linux:$ alias ls="ls -lsa"
user@linux:$ ls /home
total 12K
4,0K drwxr-xr-x  3 root root  4,0K 2020-13-01 .
4,0K drwxr-xr-x 19 root root  4,0K 2020-13-01 ..
4,0K drwx------ 77 user users 4,0K 2020-13-01 user
```
Ao executarmos `alias ls="ls -lsa"` o bash entenderá que, nesta seção, toda vez que o `ls` for executado na verdade estará a executar `ls -lsa` (`-l` usa o formato de lista longa, `-s` mostra o tamanho de cada arquivo, e `-a` apresenta arquivos ocultos).

## Criando comandos customizados

Talvez também queiramos sempre listar um diretório, sem a necessidade de ir para este diretório (com o comando `cd`), e sem digitar o diretório (por exemplo, `/tmp`). Poderíamos definir, com o `alias`, uma listagem para tal diretório:

```bash 
user@linux:$ alias ltmp="ls -la /tmp"
user@linux:$ ltmp
drwxr-xr-x  3 root root  4,0K 2020-13-01 .
drwxr-xr-x 19 root root  4,0K 2020-13-01 ..
-r--r--r-- 11 user users 4,0K 2020-13-01 arquivo1.txt
-r--r--r-- 11 root root  4,0K 2020-13-01 arquivo2.txt
-r--r--r-- 11 root root  4,0K 2020-13-01 arquivo3.txt
```

## Comando `alias` sem argumentos

O comando `alias`, executado sem argumentos, apresenta todos alias definidos na sessão. Por exemplo:


```bash 
user@linux:$ alias
alias dmesg='\dmesg --color=auto --reltime --human --nopager --decode'
alias free='\free -mht'
alias grep='\grep --color=auto'
alias tree='\tree -dirsfirst -C'
alias poweroff='sudo systemctl poweroff'
```

## `alias` temporário

No começo da postagem, foi definido um `alias` para o comando `ls`. Mas ele só é definido na sessão que estamos usando. Se fecharmos e/ou entrarmos em outra sessão `bash`, esse `alias` não irá funcionar. Existe uma forma de definí-lo durante o processo de login. Mas isso será tema para outra postagem.
