---
title: Tópico 103.1 - Trabalhando na linha de comando (14) - variáveis de ambiente importantes I - histórico
date: 2020-12-13 19:08:00
tags:
  - variáveis
  - ambiente
  - global
  - histórico
---

Nas últimas postagens, o enfoque ficou para a definição de variáveis locais, globais (com o comando `export`) e visualização das variáveis definidas (com os comando `set` e `env `) e como remover definições de variáveis.
A partir desta postagem estudaremos um pouco mais sobre variáveis importantes que podem ser pedidas no exame. 
<!-- more -->

## Variável `HISTFILE`

Quando aprendemos o comando `env`, foi possível visualizar diversas variáveis globais. Entre elas existe a variável `HISTFILE`:

```bash
user@linux~$ env | less
...
HISTFILE=/home/user/.bash_history
```

Essa variável nos diz onde fica o histórico de comandos `bash` executados até o momento. Isto é, conforme digitamos comandos em uma sessão de um _shell_ `bash`, tais comando são armazenados neste arquivo. Pode ser útil para consultar e utilizar esses comandos em um momento posterior.

## Tamanho do arquivo de histórico

De maneira grosseira, as duas variáveis definem o tamanho do arquivo e quantos comandos são armazenados no histórico. 

Mas confundir a função das variáveis pode ser fácil. Para entender melhor, recomendo a leitura [desta postagem no stackoverflow](https://stackoverflow.com/questions/19454837/bash-histsize-vs-histfilesize#19454838). As diferenças principais são: 

  - o `HISTFILESIZE` se relaciona com o *tamanho de um arquivo* armazenado no disco
  - o `HISTSIZE` se relaciona com o *tamanho de lista* armazenada na memória

Segundo a resposta dada pelo usuário _Syakur Rahman_ na postagem compartilhada, as diferenças são:


| variável       | funcionalidade                    |
| :------------- | --------------------------------: |
| `HISTFILESIZE` | é o número de linhas (ou comandos) que: (a) são permitidos em um arquivo de histórico no início da sessão e, (b) que são armazenados em um arquivo de histórico no fim de uma sessão `bash` para uso em futuras sessões  |
| `HISTSIZE`     | é o número de linhas (ou comandos) que são armazenados na memória em uma lista de histórico enquanto sua sessão `shell` está em andamento |

O usuário _Syakyr Rahman_ fornece um exemplo para entender melhor (que apenas foi traduzido e adaptado):


### Exemplo 

 Você inicia a sessão com as seguintes definições

```bash
# HISTFILESIZE=10
# HISTSIZE=10
user@linux~$
```


Seu arquivo de histórico (HISTFILE) é truncado para conter `HISTFILESIZE=10` linhas. Você escreve 50 linhas:

```bash
user@linux~$ echo 1
... 
... 
...
user@linux~$ echo 50
```

No fim dos seus 50 comandos, apenas 41 dos 50 estão na sua lista de histórico,  cujo tamanho é determinado por `HISTSIZE=10`. Você termina sua sessão:

```bash
You end your session.
user@linux~$ exit
```

Assumindo que [`histappend`](http://unix.stackexchange.com/questions/6501/ddg#6509i) não está habilitado, os comandos 41 até o 50 são salvos no seu arquivo de histórico (`HISTFILE`), que tem agora 10 comandos mantidos no início mais 10 comandos recém-escritos. Seu arquivo então é truncado para conter HISTFILESIZE=10 linhas. Agora você tem 10 comando no seu histórico - os últimos 10 que você digitou na sessão que você acabou de finalizar. Quando você iniciar uma nova sessão, você começa do zero com um HISTFILE of HISTFILESIZE=10.
