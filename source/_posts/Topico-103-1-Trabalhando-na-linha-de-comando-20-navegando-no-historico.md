---
title: Tópico 103.1 - Trabalhando na linha de comando 20 - navegando no histórico I
date: 2020-12-23 19:20:56
tags:
  - shell
  - comandos
  - histórico
  - navegação
---

Nessa postagem daremos continuidade ao assunto de propriedades do `bash` e, talvez, uma das propriedades que eu mais uso: a navegação dos comandos memorizados. 

<!-- more -->

Em uma postagem sobre a variável de ambiente que define o [arquivo de histórico](/2020/12/13/Topico-103-Linha-de-comando-14-variaveis-de-ambiente-importantes-HISTFILE/), foi possível aprender que uma das propriedades do `bash` é memorizar os comandos executados. Através deste arquivo também é possível navegarmos pelos comandos memorizados.

# Seta para cima 

Apertando a tecla com o sinal de seta para cima, podemos acessar os comandos mais antigos.

# Seta para baixo

Apertando a tecla com o sinal de seta para cima, podemos acessar os comandos mais recentes.
