---
title: Tópico 103.1 - Trabalhando na linha de comando (17) - variáveis de ambiente importantes IV - shell e terminal
date: 2020-12-15 09:59:15
tags:
  - variáveis
  - ambiente
  - global
  - shell
  - terminal
---

Na última postagem, aprendemos um pouco sobre as variáveis que descrevem o arquivos e caminhos importantes no sistema de arquivo. Nesta postagem será descrito um pouco sobre variáveis que descrevem o shell e o terminal em uso.

<!-- more -->


## Variável `SHELL`

A variável `SHELL` simplistmente nos diz qual é o executável `shell` que está sendo usado no momento. Sempre deve se referenciar a um executável cujo caminho está definido no `PATH`. Geralmente é o `shell bash` (_bourne again shell_), que está localizado em `/bin/bash`:

```bash
user@linux$~ echo $SHELL
/bin/bash
```

## Variável `TERM`

Esta variável nos diz qual é o terminal atual. Essa variável pode mudar conforme o "modo de uso", isto é, se você acessou uma máquina (`HOSTNAME`) logando com um usuário ('LOGNAME`, `USER`) através de uma interface gráfica, é comum que o resultado seja um `xterm`:


```bash
user@linux$~ echo $TERM
xterm
```

Existem outros tipos (`gnome-terminal`, `rxvt`, `urxvt`, `screen-256color`, etc.). Esses são emuladores de terminal. 

Se você acessa via linha de comando, diretamente, a variável pode retornar `tty`, que é um terminal de fato.
