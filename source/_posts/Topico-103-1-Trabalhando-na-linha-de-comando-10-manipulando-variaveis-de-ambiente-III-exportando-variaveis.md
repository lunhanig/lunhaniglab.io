---
title: Tópico 103.1 - Trabalhando na linha de comando (10): manipulando variaveis de ambiente (III) - exportando variáveis
date: 2020-07-24 08:54:13
tags:
  - export
  - variáveis
  - ambiente
  - global
  - processo
---

Na postagem anterior mostrei como declarar variáveis locais e sua relação/visibilidade com sessões de `shell`. Nesta postagem mostrarei como transformar uma variável local em global e sua relação com as sessões de `shell`.

<!-- more -->

## Comando `export`

O procedimento para transformar uma variável local em global é bem simples. É só usar o comando `export`. No nosso caso, a estrutura abaixo deve ser usada depois que declaramos uma variável local:

### Estrutura

| Comando  | argumento                |
| :------- | ----------------------:  |
| `export` | Nome da variável sem `$` |

### Exemplo 1

Abra dois emuladores de terminal. Cada um terá sua própria variável e sessão. Execute, em cada um deles, os seguintes comandos:

| Sessão 1                            | Sessão 2                              |
| :---------------------------------- | -----------------------------------:  |
| `user@linux:~$ VARIAVEL_UM=valor1`  | `user@linux:~$ VARIAVEL_DOIS=valor2`  |
| `user@linux:~$ echo $VARIAVEL_UM`   | `user@linux:~$ echo $VARIAVEL_DOIS`   |
| `valor1`                            | `valor2`                              |
| `user@linux:~$ export VARIAVEL_UM`  | `user@linux:~$ export $VARIAVEL_DOIS` |
| `user@linux:~$ echo $VARIAVEL_DOIS` | `user@linux:~$ echo $VARIAVEL_UM`     |
| ` `                                 | ` `                                   |

Note que as variáveis ainda não estão visíveis para todas as sessões no exemplo acima. Explicaremos isso no final desta postagem.

### Exemplo 2

Vamos demonstrar o uso do `export` criando uma nova sessão de `shell` dentro do `shell` que estamos usando. Perceba que a variável persiste entre as seções:

```bash
user@linux:~$ MINHA_VARIAVEL="Isso é um teste de sessão"
user@linux:~$ echo $MINHA_VARIAVEL
Isso é um teste de sessão
user@linux:~$ export MINHA_VARIAVEL
user@linux:~$ bash
user@linux:~$ echo $MINHA_VARIAVEL
Isso é um teste de sessão
user@linux:~$ exit
user@linux:~$ echo $MINHA_VARIAVEL
Isso é um teste de sessão
```

### Exemplo 3

Irei demonstrar a mesma coisa através do *script* da postagem anterior. Lembre-se que estou supondo que este *script* está localizado em `/home/user/scripts`:

<script src="https://gist.github.com/lunhg/b17f2237125336068322571019069889.js?file=test_var_session.sh&__inline=true"></script>

```bash
user@linux:~$ TESTE=testado
user@linux:~$ export TESTE
user@linux:~$ cd scripts
user@linux:~/scripts$ ls
.
..
echo_test.sh
test_var_session.sh
user@linux:~/scripts$ ./test_var_session.sh
Esse script testa a visibilidade de uma variável local. 
A variável a ser testada terá o nome TESTE
Se a variável estiver definida localmente ou globalmente, ela aparecerá aqui em baixo:
TESTE=testado
user@linux:~/scripts$
```

## Explicando

Em todos os exemplos anteriores, quando adentramos em um `shell`, estamos dentro de uma nova sessão de `shell` gerada por um `processo`. 

Um processo `shell` é uma instância (um caso específico da definição geral de *shells*). Um `shell` é considerado um *processo-pai* de um outro `shell` (*processo-filho*) quando aquele dá origem a este último. 

No exemplo 1, cada uma das sessões são processos independentes, mas geradas pelo mesmo processo. O comando `export` apenas torna global uma variável que seja herdeira da mesma `árvore de processos`. 

No exemplo 2, dentro do `bash` nós geramos um novo processo `bash`. Isto é, o segundo processo (filho), herda as variáveis globais do primeiro.

E no exemplo 3, quando executamos o *script*, a linha `#!/bin/bash` determina que um novo processo (e portanto uma nova seção) será executada, herdando as variáveis globais do primeiro proceso.
