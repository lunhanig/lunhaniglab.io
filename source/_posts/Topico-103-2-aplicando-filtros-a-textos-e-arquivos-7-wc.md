---
title: Tópico 103.2 - Aplicando Filtros a Textos e arquivos - 7 - wc
date: 2021-01-25 17:33:25
tags:
    - 103.2
    - wc
    - contagem
    - count
---

Pode ser útil contarmos quantidade de linhas ou caracteres de um arquivo (ou até mesmo de uma saída de outro comando). Para isso usamos o `wc`:

<!-- more -->

Segundo o `info wc`, o comando `wc`:

> conta o número de bytes, caracteres palavras separadas por espaço e novas linha en cada arquivo dado, ou entrada-padrão se nenhum é dado

# Resposta padrão do `wc`


Ao executarmos o comando `wc` para contagem de um arquivo ou saída-padrão, teremos:

| Campo 1            | Campo 2                | Campo 3             |
| :----------------- | :--------------------: | ------------------: |
| Quantida de linhas | Quantidade de palavras | Quantidade de bytes |

## Exemplos

### Exemplo com `echo` como entrada-padrão

Podemos redirecionar a saída de um comando `echo`, com o pipe (`|`) para contagem:

```bash
user@linux:~$ echo "Hello World" | wc 
    1       2       12
```

| Quantida de linhas | Quantidade de palavras | Quantidade de bytes |
| :----------------- | :--------------------: | ------------------: |
| 1                  | 2                      | 12                  |


### Exemplo com um arquivo

Utilize um arquivo como argumento do comando. Suponha que redirecionamos o conteúdo de `info wc` para um arquivo `wc.txt`. Desta forma poderemos contar:

```bash
user@linux:~$ info wc > wc.txt
user@linux:~$ wc wc.txt
    83  506 3325 wc.txt
```

| Quantida de linhas | Quantidade de palavras | Quantidade de bytes |
| :----------------- | :--------------------: | ------------------: |
| 83                 | 506                    | 3325                |

### Exemplo com vários arquivos

O `wc` também aceita caracteres de agrupamento, como por exemplo, o "coringa" (`*`). Suponha que você tenha alguns arquivos na sua pasta `home`. Tente experimentar:

```bash
user@linux:~$ wc *
```

E ele retornará as contagens de cada arquivo e, ao final, a soma de todos eles.

### Exemplo com um arquivo e redirecionamento a partir do `tail`

Nós podemos também pegar apenas o final de um arquivo e redirecioná-lo para o `wc`:

```bash
user@linux:~$ tail -n 10 wc.txt | wc
    10      47      305
```
Isso significa que, nas últimas 10 linhas, temos 47 palavras e 305 bytes.


## Opções mais usadas

Cada opção mostra:

| Opção curta | Opção longa   | Descrição                |
| :---------- | :-----------: | -----------------------: |
| `-c`        | `--bytes`     | quantidade de bytes      |
| `-m`        | `--char`      | quantidade de caracteres |
| `-w`        | `--words`     | quantidade de palavras   |
| `-l`        | `--lines`     | quantidade de linhas     |

### Contando apenas bytes

```bash
user@linux:~$ wc -b wc.txt
3325 wc.txt
```

### Contando apenas caracteres

```bash
user@linux:~$ wc -m wc.txt
3213 wc.txt
```

### Contando apenas palavras

```bash
user@linux:~$ wc -w wc.txt
506 wc.txt
```

### Contando apenas linhas

```bash
user@linux:~$ wc -l wc.txt
83 wc.txt
```
```bash
user@linux:~$ wc -b wc.txt
3325 wc.txt
```
