---
title: Tópico 103.1 - Trabalhando na linha de comando 22 - navegando no histórico III
date: 2020-12-28 16:55:38
tags:
  - shell
  - comandos
  - history
  - exclamação
---

Nessa postagem daremos continuidade ao assunto de propriedades do histórico. 

Na postagem anterior foi possível aprender sobre o comando `history`. Nessa postagem será apreendido comandos que utilizam o histórico para executar comandos memorizados de forma mais rápida que ficar digitando um por um, isto é, a família de comandos precedidos por um ponto de exclamação (`!`). 
 
<!-- more -->

Antes de aprendermos sobre alguns dos comandos mais usados, sugiro a leitura [desta resposta no StackExchange](https://unix.stackexchange.com/questions/3747/understanding-the-exclamation-mark-in-bash#3748) a respeito deste comando útil.

# Último comando executado (`!!`)

O comando `!!` executa o último comando memorizado. Por exemplo:

```bash
user@linux~$ ls
. .. 
user@linux~ !!
. ..
```

# Enésimo comando executado (`!N`)

Aqui _N_ é um número e `!N` significa que o `shell` executará o enésimo comando memorizado. Para saber qual é o n-ésimo comando que queremos usar, basta usar o `history`. Por exemplo:

```bash
user@linux~$ ls
. .. 
user@linux/tmp$ cd /tmp
user@linux/tmp$ cd /home/user
user@linux~$ history
  1 ls
  2 cd /tmp
  3 cd /home/user
  4 history
user@linux~$ !1
ls
. ..
user@linux~$ !2 
cd /tmp
user@linux/tmp$ !3
cd /home/user
user@linux~$ !4
history
  1 ls
  2 cd /tmp
  3 cd /home/user
  4 history
user@linux~$ 
```

# Último comando executado com uma string (`!string`)


Aqui _string_ é uma cadeia de caracteres e `!string` significa que o `shell` executará o último comando memorizado que possui a `string`. Por exemplo:

```bash
user@linux~$ ls
. .. 
user@linux/tmp$ cd /tmp
user@linux/tmp$ cd /home/user
user@linux~$ history
  1 ls
  2 cd /tmp
  3 cd /home/user
  4 history
user@linux~$ !ls
ls
. ..
user@linux~$ !tmp 
cd /tmp
user@linux/tmp$ !cd
cd /home/user
user@linux~$ !user
cd /home/user
user@linux~$ !history
history
  1 ls
  2 cd /tmp
  3 cd /home/user
  4 history
user@linux~$ 
```
