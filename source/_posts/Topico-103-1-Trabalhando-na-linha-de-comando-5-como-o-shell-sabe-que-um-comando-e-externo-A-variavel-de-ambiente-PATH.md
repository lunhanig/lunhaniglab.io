---
title: Tópico 103.1 - Trabalhando na linha de comando 5 - como o shell sabe que um comando é externo? A variável de ambiente PATH
date: 2020-07-20 21:17:20
tags:
  - comando
  - externo
  - variável
  - ambiente
  - PATH
---

Na [postagem anterior](/2020/07/19/Topico-103-Linha-de-comando-4-o-que-e-um-comando-comando-type), vimos que comandos podem ser internos, externos e *scripts*. Nesta postagem aprenderemos um pouco mais sobre os comandos externos: como o `shell` sabe que um comando externo é externos?

<!-- more -->

## A variável de ambiente `PATH`

Na postagem sobre o comando [echo](/2020/07/18/Topico-103-Linha-de-comando-3-comando-echo/), falamos um pouco sobre o uso de *variáveis de ambiente*.

Existem uma muito importante, que você deve lembrar, não apenas para o exame, mas para o uso diário de seu sistema Linux: a variável `PATH`.

### O que é o PATH

Podemos entender a variável `PATH` através de sua tradução: *caminho*. Isto é, a variável `PATH` guarda alguma informação importante sobre uma trajetória. E o que seriam essa trajetória?

Segundo [Leonardo Amorim](https://www.vivaolinux.com.br/artigo/O-que-e-PATH-como-funciona-e-como-trabalhar-com-ele), o `PATH` 

> é uma variável do sistema Linux que indica trajetória (tradução do inglês) dos binários (executáveis dos programas), que podem ser executados sem indicar o caminho completo (geralmente ele é muito longo) da onde eles estão.

Lembra do comando `type clear`? ele indica que `clear é /usr/bin/clear`. Em um `shell`, executarmos `clear` ou `/usr/bin/clear` dá na mesma, pois, através da variável de `PATH`, o `shell` já sabe que o primeiro faz referência ao segundo.

### Conhecendo o PATH

Vamos usar o comando `echo`, seguido de `$PATH`, para conhecermos mais sobre a variável de ambiente `PATH` (o resultado pode variar, em um sistema meu, está assim):

```bash
user@linux:~$ echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl
```

#### Destrinchando o PATH

O `PATH` é uma variável que memoriza diretórios do sistema, através de uma `string`, separada por `:`. No comando acima, a `string` indica 8 diretórios diferentes.

Quando digitamos um comando, por exemplo `echo`, o `shell` consulta o `PATH` e  verifica se existe, no diretório `/usr/local/bin`, o comando `echo`. 

Se não encontrar, ele irá procurar o comando `echo` em `/usr/bin`. Nós vimos, através do comando `type echo` que `echo é /usr/bin/echo`. Portando o `shell` irá parar sua consulta e executará o comando `echo` como `/usr/bin/echo`.

Para qualquer comando digitado no `shell` esse procedimento será feito, até que o comando seja encontrado.

